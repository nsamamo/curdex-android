package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import com.nichie.curdex.presentation.wallet.BackupWalletActivity
import com.nichie.curdex.presentation.wallet.BackupWalletFragment
import com.nichie.curdex.presentation.wallet.BackupWalletFragmentNext
import com.nichie.curdex.util.di.scope.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface BackupWalletActivityModule {

    @Binds
    fun bindsAppCompatActivity(backupWalletActivity: BackupWalletActivity): AppCompatActivity

    @FragmentScoped
    @ContributesAndroidInjector
    fun contributeBackupWalletFragment(): BackupWalletFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun contributeBackupWalletFragmentNext(): BackupWalletFragmentNext
}