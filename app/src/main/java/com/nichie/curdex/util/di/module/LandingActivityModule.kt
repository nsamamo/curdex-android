package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.nichie.curdex.presentation.landing.LandingActivity
import com.nichie.curdex.presentation.landing.LandingActivityViewModel
import com.nichie.curdex.presentation.landing.LandingFragment
import com.nichie.curdex.presentation.landing.LandingViewModel
import com.nichie.curdex.presentation.main.kybercode.KyberCodeFragment
import com.nichie.curdex.presentation.main.kybercode.KyberCodeViewModel
import com.nichie.curdex.util.di.ViewModelKey
import com.nichie.curdex.util.di.scope.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface LandingActivityModule {

    @Binds
    fun bindsAppCompatActivity(landingActivity: LandingActivity): AppCompatActivity

    @FragmentScoped
    @ContributesAndroidInjector
    fun contributeLandingFragment(): LandingFragment

    @FragmentScoped
    @ContributesAndroidInjector
    fun contributeKyberCodeFragment(): KyberCodeFragment

    @Binds
    @IntoMap
    @ViewModelKey(LandingViewModel::class)
    fun bindLandingViewModel(
        landingViewModel: LandingViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LandingActivityViewModel::class)
    fun bindLandingActivityViewModel(
        landingActivityViewModel: LandingActivityViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(KyberCodeViewModel::class)
    fun bindKyberCodeViewModel(
        kyberCodeViewModel: KyberCodeViewModel
    ): ViewModel
}