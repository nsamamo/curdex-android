package com.nichie.curdex.util.di.module

import com.nichie.curdex.presentation.common.AlertActivity
import com.nichie.curdex.presentation.common.AlertWithoutIconActivity
import com.nichie.curdex.presentation.landing.LandingActivity
import com.nichie.curdex.presentation.main.MainActivity
import com.nichie.curdex.presentation.main.balance.send.SendConfirmActivity
import com.nichie.curdex.presentation.main.profile.TermConditionActivity
import com.nichie.curdex.presentation.main.swap.PromoPaymentConfirmActivity
import com.nichie.curdex.presentation.main.swap.PromoSwapConfirmActivity
import com.nichie.curdex.presentation.main.swap.SwapConfirmActivity
import com.nichie.curdex.presentation.setting.PassCodeLockActivity
import com.nichie.curdex.presentation.splash.SplashActivity
import com.nichie.curdex.presentation.wallet.BackupWalletActivity
import com.nichie.curdex.presentation.wallet.ImportWalletActivity
import com.nichie.curdex.presentation.wallet.VerifyBackupWordActivity
import com.nichie.curdex.util.di.scope.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class
        ]
    )
    fun contributeMainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            SplashActivityModule::class
        ]
    )
    fun contributeSplashActivity(): SplashActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            LandingActivityModule::class
        ]
    )
    fun contributeLandingActivity(): LandingActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            BackupWalletActivityModule::class
        ]
    )
    fun contributeBackupWalletActivity(): BackupWalletActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            VerifyBackupWordActivityModule::class
        ]
    )
    fun contributeVerifyBackupWordActivity(): VerifyBackupWordActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            ImportWalletActivityModule::class
        ]
    )
    fun contributeImportWalletActivity(): ImportWalletActivity

    @ActivityScoped
    @ContributesAndroidInjector()
    fun contributeAlertActivity(): AlertActivity

    @ActivityScoped
    @ContributesAndroidInjector()
    fun contributeInsufficientAlertActivity(): AlertWithoutIconActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            SwapConfirmActivityModule::class
        ]
    )
    fun contributeSwapConfirmActivity(): SwapConfirmActivity


    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            SendConfirmActivityModule::class
        ]
    )
    fun contributeSendConfirmActivity(): SendConfirmActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            TermConditionActivityModule::class
        ]
    )
    fun contributeTermConditionActivity(): TermConditionActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            PromoPaymentActivityModule::class
        ]
    )
    fun contributePromoPaymentConfirmActivity(): PromoPaymentConfirmActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            PromoSwapActivityModule::class
        ]
    )
    fun contributePromoSwapConfirmActivity(): PromoSwapConfirmActivity

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            PassCodeLockActivityModule::class
        ]
    )
    fun contributePassCodeLockActivity(): PassCodeLockActivity
}
