package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.nichie.curdex.presentation.main.balance.send.SendConfirmActivity
import com.nichie.curdex.presentation.main.balance.send.SendConfirmViewModel
import com.nichie.curdex.util.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface SendConfirmActivityModule {

    @Binds
    fun bindsAppCompatActivity(sendConfirmActivity: SendConfirmActivity): AppCompatActivity

    @Binds
    @IntoMap
    @ViewModelKey(SendConfirmViewModel::class)
    fun bindSendConfirmViewModel(
        sendConfirmViewModel: SendConfirmViewModel
    ): ViewModel
}