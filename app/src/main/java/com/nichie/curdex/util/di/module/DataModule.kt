package com.nichie.curdex.util.di.module

import android.content.Context
import com.nichie.curdex.data.api.home.ChartApi
import com.nichie.curdex.data.api.home.CurrencyApi
import com.nichie.curdex.data.api.home.LimitOrderApi
import com.nichie.curdex.data.api.home.PromoApi
import com.nichie.curdex.data.api.home.SwapApi
import com.nichie.curdex.data.api.home.TokenApi
import com.nichie.curdex.data.api.home.TransactionApi
import com.nichie.curdex.data.api.home.UserApi
import com.nichie.curdex.data.db.AlertDao
import com.nichie.curdex.data.db.ContactDao
import com.nichie.curdex.data.db.LimitOrderDao
import com.nichie.curdex.data.db.LocalLimitOrderDao
import com.nichie.curdex.data.db.OrderFilterDao
import com.nichie.curdex.data.db.PassCodeDao
import com.nichie.curdex.data.db.PendingBalancesDao
import com.nichie.curdex.data.db.RateDao
import com.nichie.curdex.data.db.SendDao
import com.nichie.curdex.data.db.SwapDao
import com.nichie.curdex.data.db.TokenDao
import com.nichie.curdex.data.db.TransactionDao
import com.nichie.curdex.data.db.TransactionFilterDao
import com.nichie.curdex.data.db.UnitDao
import com.nichie.curdex.data.db.UserDao
import com.nichie.curdex.data.db.WalletDao
import com.nichie.curdex.data.mapper.AlertMapper
import com.nichie.curdex.data.mapper.CapMapper
import com.nichie.curdex.data.mapper.ChartMapper
import com.nichie.curdex.data.mapper.FeeMapper
import com.nichie.curdex.data.mapper.GasMapper
import com.nichie.curdex.data.mapper.OrderMapper
import com.nichie.curdex.data.mapper.PromoMapper
import com.nichie.curdex.data.mapper.RateMapper
import com.nichie.curdex.data.mapper.TokenMapper
import com.nichie.curdex.data.mapper.TransactionMapper
import com.nichie.curdex.data.mapper.UserMapper
import com.nichie.curdex.data.repository.AlertDataRepository
import com.nichie.curdex.data.repository.BalanceDataRepository
import com.nichie.curdex.data.repository.ContactDataRepository
import com.nichie.curdex.data.repository.LimitOrderDataRepository
import com.nichie.curdex.data.repository.MnemonicDataRepository
import com.nichie.curdex.data.repository.SettingDataRepository
import com.nichie.curdex.data.repository.SwapDataRepository
import com.nichie.curdex.data.repository.TokenDataRepository
import com.nichie.curdex.data.repository.TransactionDataRepository
import com.nichie.curdex.data.repository.UserDataRepository
import com.nichie.curdex.data.repository.WalletDataRepository
import com.nichie.curdex.data.repository.datasource.storage.StorageMediator
import com.nichie.curdex.domain.repository.AlertRepository
import com.nichie.curdex.domain.repository.BalanceRepository
import com.nichie.curdex.domain.repository.ContactRepository
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.repository.MnemonicRepository
import com.nichie.curdex.domain.repository.SettingRepository
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.repository.TokenRepository
import com.nichie.curdex.domain.repository.TransactionRepository
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.util.TokenClient
import dagger.Module
import dagger.Provides
import org.bitcoinj.crypto.MnemonicCode
import javax.inject.Singleton

@Module
object DataModule {
    @Singleton
    @Provides
    @JvmStatic
    fun provideWalletRepository(
        context: Context,
        walletDao: WalletDao,
        unitDao: UnitDao,
        tokenDao: TokenDao,
        promoApi: PromoApi,
        promoMapper: PromoMapper,
        swapDao: SwapDao,
        sendDao: SendDao,
        limitOrderDao: LocalLimitOrderDao
    ): WalletRepository =
        WalletDataRepository(
            context,
            walletDao,
            unitDao,
            tokenDao,
            promoApi,
            promoMapper,
            swapDao,
            sendDao,
            limitOrderDao
        )

    @Singleton
    @Provides
    @JvmStatic
    fun provideBalanceRepository(
        context: Context,
        api: TokenApi,
        currencyApi: CurrencyApi,
        tokenMapper: TokenMapper,
        client: TokenClient,
        tokenDao: TokenDao,
        walletDao: WalletDao,
        swapDao: SwapDao,
        sendDao: SendDao,
        localLimitOrderDao: LocalLimitOrderDao

    ): BalanceRepository =
        BalanceDataRepository(
            context,
            api,
            currencyApi,
            tokenMapper,
            client,
            tokenDao,
            walletDao,
            swapDao,
            sendDao,
            localLimitOrderDao
        )

    @Singleton
    @Provides
    @JvmStatic
    fun provideTokenRepository(
        client: TokenClient,
        api: SwapApi,
        chartApi: ChartApi,
        rateDao: RateDao,
        tokenDao: TokenDao,
        rateMapper: RateMapper,
        chartMapper: ChartMapper,
        context: Context
    ): TokenRepository =
        TokenDataRepository(
            client,
            api,
            chartApi,
            rateDao,
            tokenDao,
            rateMapper,
            chartMapper,
            context
        )

    @Singleton
    @Provides
    @JvmStatic
    fun provideMnemonicRepository(mnemonicCode: MnemonicCode): MnemonicRepository =
        MnemonicDataRepository(mnemonicCode)

    @Singleton
    @Provides
    @JvmStatic
    fun provideSwapRepository(
        context: Context,
        swapDao: SwapDao,
        tokenDao: TokenDao,
        sendDao: SendDao,
        contactDao: ContactDao,
        api: SwapApi,
        mapper: GasMapper,
        capMapper: CapMapper,
        tokenClient: TokenClient,
        transactionDao: TransactionDao,
        userDao: UserDao,
        userApi: UserApi,
        userMapper: UserMapper
    ): SwapRepository =
        SwapDataRepository(
            context,
            swapDao,
            tokenDao,
            sendDao,
            contactDao,
            api,
            mapper,
            capMapper,
            tokenClient,
            transactionDao,
            userDao,
            userApi,
            userMapper
        )

    @Singleton
    @Provides
    @JvmStatic
    fun provideContactRepository(
        contactDao: ContactDao,
        sendDao: SendDao
    ): ContactRepository =
        ContactDataRepository(contactDao, sendDao)


    @Singleton
    @Provides
    @JvmStatic
    fun provideTransactionRepository(
        api: TransactionApi,
        transactionDao: TransactionDao,
        mapper: TransactionMapper,
        tokenClient: TokenClient,
        tokenDao: TokenDao,
        swapDao: SwapDao,
        sendDao: SendDao,
        limitOrderDao: LocalLimitOrderDao,
        transactionFilterDao: TransactionFilterDao,
        context: Context
    ): TransactionRepository =
        TransactionDataRepository(
            api,
            transactionDao,
            mapper,
            tokenClient,
            tokenDao,
            swapDao,
            sendDao,
            limitOrderDao,
            transactionFilterDao,
            context
        )


    @Singleton
    @Provides
    @JvmStatic
    fun provideUserRepository(
        api: UserApi,
        userDao: UserDao,
        storageMediator: StorageMediator,
        userMapper: UserMapper,
        alertDao: AlertDao
    ): UserRepository =
        UserDataRepository(api, userDao, storageMediator, userMapper, alertDao)


    @Singleton
    @Provides
    @JvmStatic
    fun provideLimitOrderRepository(
        context: Context,
        tokenDao: TokenDao,
        localLimitOrderDao: LocalLimitOrderDao,
        orderFilterDao: OrderFilterDao,
        dao: LimitOrderDao,
        pendingBalancesDao: PendingBalancesDao,
        api: LimitOrderApi,
        mapper: OrderMapper,
        feeMapper: FeeMapper,
        tokenClient: TokenClient
    ): LimitOrderRepository =
        LimitOrderDataRepository(
            context,
            dao,
            localLimitOrderDao,
            orderFilterDao,
            tokenDao,
            pendingBalancesDao,
            api,
            tokenClient,
            mapper,
            feeMapper
        )


    @Singleton
    @Provides
    @JvmStatic
    fun provideAlertRepository(
        alertDao: AlertDao,
        tokenDao: TokenDao,
        userApi: UserApi,
        alertMapper: AlertMapper
    ): AlertRepository =
        AlertDataRepository(
            alertDao,
            tokenDao,
            userApi,
            alertMapper
        )

    @Singleton
    @Provides
    @JvmStatic
    fun providePassCodeRepository(
        passCodeDao: PassCodeDao
    ): SettingRepository =
        SettingDataRepository(
            passCodeDao
        )
}