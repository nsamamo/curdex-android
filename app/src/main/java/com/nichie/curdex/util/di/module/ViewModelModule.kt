package com.nichie.curdex.util.di.module

import androidx.lifecycle.ViewModelProvider
import com.nichie.curdex.util.di.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
