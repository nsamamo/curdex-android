package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.nichie.curdex.presentation.setting.PassCodeLockActivity
import com.nichie.curdex.presentation.setting.PassCodeLockViewModel
import com.nichie.curdex.util.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface PassCodeLockActivityModule {

    @Binds
    fun bindsAppCompatActivity(landingActivity: PassCodeLockActivity): AppCompatActivity


    @Binds
    @IntoMap
    @ViewModelKey(PassCodeLockViewModel::class)
    fun bindPassCodeLockViewModel(
        passCodeLockViewModel: PassCodeLockViewModel
    ): ViewModel
}