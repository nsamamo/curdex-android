package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import com.nichie.curdex.presentation.wallet.VerifyBackupWordActivity
import dagger.Binds
import dagger.Module

@Module
interface VerifyBackupWordActivityModule {

    @Binds
    fun bindsAppCompatActivity(verifyBackupWordActivity: VerifyBackupWordActivity): AppCompatActivity
}