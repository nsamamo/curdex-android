package com.nichie.curdex.util.di.module

import androidx.appcompat.app.AppCompatActivity
import com.nichie.curdex.presentation.main.profile.TermConditionActivity
import dagger.Binds
import dagger.Module

@Module
interface TermConditionActivityModule {

    @Binds
    fun bindsAppCompatActivity(termConditionActivity: TermConditionActivity): AppCompatActivity

}