package com.nichie.curdex.domain.usecase.alert

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.repository.AlertRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetAlertUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val alertRepository: AlertRepository
) : SequentialUseCase<GetAlertUseCase.Param, Alert>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<Alert> {
        return alertRepository.getAlert(param)
    }

    class Param(val alertId: Long)
}
