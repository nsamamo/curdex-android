package com.nichie.curdex.domain.usecase.swap

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import org.web3j.protocol.core.methods.response.EthEstimateGas
import java.math.BigInteger
import javax.inject.Inject

class EstimateGasUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val swapRepository: SwapRepository
) : SequentialUseCase<EstimateGasUseCase.Param, EthEstimateGas>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<EthEstimateGas> {
        return swapRepository.estimateGas(param)
    }

    class Param(
        val wallet: Wallet,
        val tokenSource: Token,
        val tokenDest: Token,
        val sourceAmount: String,
        val minConversionRate: BigInteger
    )
}
