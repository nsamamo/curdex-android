package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Fee
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetLimitOrderFeeUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : FlowableUseCase<GetLimitOrderFeeUseCase.Param, Fee>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<Fee> {
        return limitOrderRepository.getLimitOrderFee(param)
    }

    class Param(
        val sourceToken: Token,
        val destToken: Token,
        val sourceAmount: String,
        val destAmount: String,
        val userAddress: String
    )
}
