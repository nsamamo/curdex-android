package com.nichie.curdex.domain.usecase.token

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.BalanceRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class GetTokensBalanceUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val balanceRepository: BalanceRepository
) : CompletableUseCase<GetTokensBalanceUseCase.Param, Any?>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return balanceRepository.getTokenBalances(param)
    }

    class Param(val wallet: Wallet, val tokens: List<Token>)
}
