package com.nichie.curdex.domain.model

import com.nichie.curdex.data.api.limitorder.EligibleAddressEntity


data class EligibleAddress(
    val success: Boolean = false,
    val eligibleAddress: Boolean = false
) {
    constructor(eligibleAddressEntity: EligibleAddressEntity) : this(
        eligibleAddressEntity.success, eligibleAddressEntity.eligibleAddress
    )
}