package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.usecase.contact.DeleteContactUseCase
import com.nichie.curdex.domain.usecase.contact.GetContactUseCase
import com.nichie.curdex.domain.usecase.contact.SaveContactUseCase
import io.reactivex.Completable
import io.reactivex.Flowable

interface ContactRepository {

    fun getContacts(param: GetContactUseCase.Param): Flowable<List<Contact>>

    fun saveContact(param: SaveContactUseCase.Param): Completable

    fun deleteContact(param: DeleteContactUseCase.Param): Completable
}