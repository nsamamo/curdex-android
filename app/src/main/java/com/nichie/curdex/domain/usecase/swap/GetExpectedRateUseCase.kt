package com.nichie.curdex.domain.usecase.swap

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.repository.TokenRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetExpectedRateUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val tokenRepository: TokenRepository
) : FlowableUseCase<GetExpectedRateUseCase.Param, List<String>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<List<String>> {
        return tokenRepository.getExpectedRate(param)
    }

    class Param(
        val walletAddress: String,
        val tokenSource: Token,
        val tokenDest: Token,
        val srcAmount: String
    )


}
