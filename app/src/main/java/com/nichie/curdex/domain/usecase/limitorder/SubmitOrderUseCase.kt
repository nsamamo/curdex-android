package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.LimitOrderResponse
import com.nichie.curdex.domain.model.LocalLimitOrder
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class SubmitOrderUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : SequentialUseCase<SubmitOrderUseCase.Param, LimitOrderResponse>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<LimitOrderResponse> {
        return limitOrderRepository.submitOrder(param)
    }

    class Param(
        val localLimitOrder: LocalLimitOrder,
        val wallet: Wallet
    )
}
