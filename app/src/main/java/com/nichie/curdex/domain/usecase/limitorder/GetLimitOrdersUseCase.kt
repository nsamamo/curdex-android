package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Order
import com.nichie.curdex.domain.model.OrderFilter
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.MergeDelayErrorUseCase
import com.nichie.curdex.presentation.main.limitorder.OrdersWrapper
import com.nichie.curdex.util.rx.operator.zipWithFlatMap
import io.reactivex.Flowable
import io.reactivex.rxkotlin.Flowables
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GetLimitOrdersUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : MergeDelayErrorUseCase<String?, OrdersWrapper>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: String?): Flowable<OrdersWrapper> {
        return Flowables.zip(
            limitOrderRepository.getOrderFilter(),
            limitOrderRepository.getLimitOrders()
        ) { filter, orders ->
            OrdersWrapper(filterOrders(orders, filter), filter.oldest)

        }.repeatWhen {
            it.delay(10, TimeUnit.SECONDS)
        }
            .retryWhen { throwable ->
                throwable.compose(zipWithFlatMap())
            }
    }

    private fun filterOrders(
        orders: List<Order>,
        orderFilter: OrderFilter
    ): List<Order> {
        return orders
            .filter {
                !orderFilter.unSelectedStatus.map { it.toLowerCase() }.contains(it.status.toLowerCase()) &&
                    !orderFilter.unSelectedPairs.contains(Pair(it.src, it.dst)) &&
                    !orderFilter.unSelectedAddresses.contains(it.userAddr)
            }

    }
}
