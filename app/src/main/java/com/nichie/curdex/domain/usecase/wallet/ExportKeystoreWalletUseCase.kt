package com.nichie.curdex.domain.usecase.wallet

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class ExportKeystoreWalletUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val walletRepository: WalletRepository
) : SequentialUseCase<ExportKeystoreWalletUseCase.Param, String>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseSingle(param: Param): Single<String> {
        return walletRepository.exportKeystore(param)
    }

    class Param(
        val password: String,
        val wallet: Wallet
    )
}
