package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.usecase.balance.UpdateBalanceUseCase
import com.nichie.curdex.domain.usecase.token.GetBalancePollingUseCase
import com.nichie.curdex.domain.usecase.token.GetOtherBalancePollingUseCase
import com.nichie.curdex.domain.usecase.token.GetOtherTokenBalancesUseCase
import com.nichie.curdex.domain.usecase.token.GetTokensBalanceUseCase
import com.nichie.curdex.domain.usecase.token.PrepareBalanceUseCase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface BalanceRepository {
    fun getChange24hPolling(param: GetBalancePollingUseCase.Param): Flowable<List<Token>>
    fun getOthersBalancePolling(param: GetOtherBalancePollingUseCase.Param): Flowable<List<Token>>
    fun getChange24h(): Flowable<List<Token>>
    fun getTokenBalance(token: Token): Completable
    fun getOtherTokenBalances(param: GetOtherTokenBalancesUseCase.Param): Completable
    fun getTokenBalances(param: GetTokensBalanceUseCase.Param): Completable
    fun getBalance(param: PrepareBalanceUseCase.Param = PrepareBalanceUseCase.Param()): Single<List<Token>>
    fun updateBalance(param: UpdateBalanceUseCase.Param): Completable
}
