package com.nichie.curdex.domain.repository

import io.reactivex.Single

interface MnemonicRepository {
    fun create12wordsAccount(numberOfWords: Int): Single<List<String>>
}