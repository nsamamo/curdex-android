package com.nichie.curdex.domain.model

import com.nichie.curdex.data.api.user.KycResponseStatusEntity


data class KycResponseStatus(
    val success: Boolean = false,
    val reason: List<String> = listOf()
) {
    constructor(entity: KycResponseStatusEntity) : this(
        entity.success, entity.reason
    )
}