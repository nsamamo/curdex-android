package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Order
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetRelatedLimitOrdersUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : FlowableUseCase<GetRelatedLimitOrdersUseCase.Param, List<Order>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<List<Order>> {
        return limitOrderRepository.getRelatedLimitOrders(param)
    }

    class Param(
        val walletAddress: String,
        val tokenSource: Token,
        val tokenDest: Token,
        val status: String? = null
    )
}
