package com.nichie.curdex.domain.usecase.setting

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.VerifyStatus
import com.nichie.curdex.domain.repository.SettingRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class VerifyPinUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val settingRepository: SettingRepository
) : SequentialUseCase<VerifyPinUseCase.Param, VerifyStatus>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<VerifyStatus> {
        return settingRepository.verifyPin(param)
    }

    class Param(val pin: String, val remainNum: Int, val time: Long)
}
