package com.nichie.curdex.domain.usecase.token

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Chart
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.repository.TokenRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import com.nichie.curdex.presentation.main.balance.chart.ChartType
import io.reactivex.Single
import javax.inject.Inject

class GetChartDataForTokenUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val tokenRepository: TokenRepository
) : SequentialUseCase<GetChartDataForTokenUseCase.Param, Chart>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<Chart> {
        return tokenRepository.getChartData(param)
    }

    class Param(val token: Token, val charType: ChartType, val rateType: String = "mid")


}
