package com.nichie.curdex.domain.usecase.alert

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.repository.AlertRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class DeleteAlertsUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val alertRepository: AlertRepository
) : SequentialUseCase<DeleteAlertsUseCase.Param, Response<Void>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<Response<Void>> {
        return alertRepository.deleteAlert(param)
    }


    class Param(val alert: Alert)
}
