package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.PassCode
import com.nichie.curdex.domain.model.VerifyStatus
import com.nichie.curdex.domain.usecase.setting.SavePinUseCase
import com.nichie.curdex.domain.usecase.setting.VerifyPinUseCase
import io.reactivex.Completable
import io.reactivex.Single

interface SettingRepository {
    fun savePin(param: SavePinUseCase.Param): Completable

    fun verifyPin(param: VerifyPinUseCase.Param): Single<VerifyStatus>

    fun getPin(): Single<PassCode>
}