package com.nichie.curdex.domain.usecase.swap

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.EstimateAmount
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class EstimateAmountUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val swapRepository: SwapRepository
) : SequentialUseCase<EstimateAmountUseCase.Param, EstimateAmount>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<EstimateAmount> {
        return swapRepository.estimateAmount(param)
    }

    class Param(
        val source: String,
        val dest: String,
        val destAmount: String
    )


}
