package com.nichie.curdex.domain.usecase.token

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.BalanceRepository
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class PreloadUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val balanceRepository: BalanceRepository,
    private val userRepository: UserRepository,
    private val walletRepository: WalletRepository
) : FlowableUseCase<String?, Wallet>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: String?): Flowable<Wallet> {
        return balanceRepository.getBalance().toFlowable().flatMap {
            walletRepository.getSelectedWallet()
        }
    }
}
