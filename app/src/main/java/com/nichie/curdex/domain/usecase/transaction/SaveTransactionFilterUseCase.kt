package com.nichie.curdex.domain.usecase.transaction

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.OrderFilter
import com.nichie.curdex.domain.model.TransactionFilter
import com.nichie.curdex.domain.repository.TransactionRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class SaveTransactionFilterUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val transactionRepository: TransactionRepository
) : CompletableUseCase<SaveTransactionFilterUseCase.Param, OrderFilter>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return transactionRepository.saveTransactionFilter(param)
    }

    class Param(val transactionFilter: TransactionFilter)
}
