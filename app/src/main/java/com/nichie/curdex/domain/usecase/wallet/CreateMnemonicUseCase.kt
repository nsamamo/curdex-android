package com.nichie.curdex.domain.usecase.wallet

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.repository.MnemonicRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class CreateMnemonicUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val mnemonicRepository: MnemonicRepository
) : SequentialUseCase<Int, List<String>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseSingle(param: Int): Single<List<String>> {

        return mnemonicRepository.create12wordsAccount(param)
    }
}
