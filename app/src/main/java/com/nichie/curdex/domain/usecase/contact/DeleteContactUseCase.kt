package com.nichie.curdex.domain.usecase.contact

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.repository.ContactRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class DeleteContactUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val contactRepository: ContactRepository
) : CompletableUseCase<DeleteContactUseCase.Param, Any?>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return contactRepository.deleteContact(param)
    }

    class Param(val contact: Contact)
}
