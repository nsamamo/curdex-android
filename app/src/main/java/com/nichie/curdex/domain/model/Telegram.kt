package com.nichie.curdex.domain.model


import com.nichie.curdex.data.api.alert.TelegramEntity

data class Telegram(
    val id: String = "",
    val active: Boolean = false,
    val name: String = ""
) {
    constructor(entity: TelegramEntity) : this(entity.id, entity.active, entity.name)
}