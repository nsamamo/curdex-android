package com.nichie.curdex.domain.usecase.profile

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import com.nichie.curdex.presentation.main.profile.kyc.KycInfoType
import io.reactivex.Completable
import javax.inject.Inject

class SaveKycInfoUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val userRepository: UserRepository
) : CompletableUseCase<SaveKycInfoUseCase.Param, Any?>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return userRepository.save(param)
    }

    class Param(val value: String, val kycInfoType: KycInfoType)
}
