package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.model.LeaderBoard
import com.nichie.curdex.domain.usecase.alert.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.Response

interface AlertRepository {

    fun saveAlertToken(param: SaveAlertTokenUseCase.Param): Completable

    fun getCurrentAlert(param: GetCurrentAlertUseCase.Param): Flowable<Alert>

    fun createOrUpdateAlert(param: CreateOrUpdateAlertUseCase.Param): Single<Alert>

    fun deleteAlert(param: DeleteAlertsUseCase.Param): Single<Response<Void>>

    fun getLeaderBoardAlert(): Single<LeaderBoard>

    fun getCampaignResult(): Single<LeaderBoard>

    fun getAlert(param: GetAlertUseCase.Param): Single<Alert>

    fun updateCurrentAlert(param: UpdateCurrentAlertUseCase.Param): Completable
}