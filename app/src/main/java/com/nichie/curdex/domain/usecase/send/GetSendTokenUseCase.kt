package com.nichie.curdex.domain.usecase.send

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Send
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetSendTokenUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val swapRepository: SwapRepository
) : FlowableUseCase<GetSendTokenUseCase.Param, Send>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<Send> {
        return swapRepository.getSendData(param)
    }

    class Param(val wallet: Wallet)
}
