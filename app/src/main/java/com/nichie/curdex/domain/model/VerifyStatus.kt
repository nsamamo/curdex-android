package com.nichie.curdex.domain.model


data class VerifyStatus(
    val success: Boolean = false,
    val message: String = "",
    val isEmptyWallet: Boolean = false
)