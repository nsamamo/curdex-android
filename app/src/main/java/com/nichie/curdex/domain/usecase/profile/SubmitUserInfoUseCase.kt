package com.nichie.curdex.domain.usecase.profile

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.KycResponseStatus
import com.nichie.curdex.domain.model.UserInfo
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class SubmitUserInfoUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val userRepository: UserRepository
) : SequentialUseCase<SubmitUserInfoUseCase.Param, KycResponseStatus>(schedulerProvider) {


    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<KycResponseStatus> {
        return userRepository.submit(param)
    }


    class Param(val user: UserInfo)
}
