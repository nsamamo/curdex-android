package com.nichie.curdex.domain.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.nichie.curdex.data.db.ListStringConverter
import com.nichie.curdex.data.db.TransactionTypesConverter
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "transaction_filter")
@Parcelize
data class TransactionFilter(
    @PrimaryKey
    val walletAddress: String = "",
    val from: String = "",
    val to: String = "",
    @TypeConverters(TransactionTypesConverter::class)
    val types: List<Transaction.TransactionType> = listOf(),
    @TypeConverters(ListStringConverter::class)
    val tokens: List<String> = listOf()
) : Parcelable