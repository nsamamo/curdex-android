package com.nichie.curdex.domain.usecase.alert

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.repository.AlertRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class SaveAlertTokenUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val alertRepository: AlertRepository
) : CompletableUseCase<SaveAlertTokenUseCase.Param, Any?>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return alertRepository.saveAlertToken(param)
    }

    class Param(val walletAddress: String, val token: Token, val alert: Alert?)
}
