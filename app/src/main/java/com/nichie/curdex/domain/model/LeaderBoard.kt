package com.nichie.curdex.domain.model


import com.nichie.curdex.data.api.alert.CampaignInfoEntity
import com.nichie.curdex.data.api.alert.LeaderBoardEntity

data class LeaderBoard(
    val currentUser: CurrentUser = CurrentUser(),
    val `data`: List<Alert> = listOf(),
    val campaignInfo: CampaignInfo = CampaignInfo(),
    val lastCampaignTitle: String = ""
) {
    constructor(entity: LeaderBoardEntity) : this(
        CurrentUser(entity.currentUserEntity),
        entity.data.map {
            Alert(it)
        },
        CampaignInfo(entity.campaignInfo ?: CampaignInfoEntity()),
        entity.lastCampaignTitle ?: ""
    )
}