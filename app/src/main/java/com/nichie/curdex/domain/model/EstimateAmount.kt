package com.nichie.curdex.domain.model


data class EstimateAmount(
    val success: Boolean = false,
    val value: String = ""
)