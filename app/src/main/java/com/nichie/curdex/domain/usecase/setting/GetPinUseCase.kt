package com.nichie.curdex.domain.usecase.setting

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.PassCode
import com.nichie.curdex.domain.repository.SettingRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetPinUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val settingRepository: SettingRepository
) : SequentialUseCase<String?, PassCode>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: String?): Single<PassCode> {
        return settingRepository.getPin()
    }
}
