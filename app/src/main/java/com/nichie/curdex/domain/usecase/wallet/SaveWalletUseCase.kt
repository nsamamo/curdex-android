package com.nichie.curdex.domain.usecase.wallet

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.model.Word
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class SaveWalletUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val walletRepository: WalletRepository
) : CompletableUseCase<SaveWalletUseCase.Param, Pair<Wallet, List<Word>>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseCompletable(param: Param): Completable {
        return walletRepository.saveWallet(param)
    }

    class Param(
        val wallet: Wallet
    )
}
