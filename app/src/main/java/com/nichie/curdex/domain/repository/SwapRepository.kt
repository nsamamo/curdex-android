package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.*
import com.nichie.curdex.domain.usecase.send.GetSendTokenUseCase
import com.nichie.curdex.domain.usecase.send.SaveSendTokenUseCase
import com.nichie.curdex.domain.usecase.send.SaveSendUseCase
import com.nichie.curdex.domain.usecase.send.TransferTokenUseCase
import com.nichie.curdex.domain.usecase.swap.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.web3j.protocol.core.methods.response.EthEstimateGas

interface SwapRepository {

    fun getSwapData(param: GetSwapDataUseCase.Param): Flowable<Swap>

    fun getSendData(param: GetSendTokenUseCase.Param): Flowable<Send>

    fun saveSwapData(param: SaveSwapDataTokenUseCase.Param): Completable

    fun saveSwap(param: SaveSwapUseCase.Param): Completable

    fun saveSend(param: SaveSendTokenUseCase.Param): Completable

    fun saveSend(param: SaveSendUseCase.Param): Completable

    fun getGasPrice(): Flowable<Gas>

    fun getCap(param: GetCapUseCase.Param): Single<Cap>

    fun estimateAmount(param: EstimateAmountUseCase.Param): Single<EstimateAmount>

    fun estimateGas(param: EstimateGasUseCase.Param): Single<EthEstimateGas>

    fun swapToken(param: SwapTokenUseCase.Param): Single<ResponseStatus>

    fun transferToken(param: TransferTokenUseCase.Param): Single<ResponseStatus>

    fun estimateGas(param: EstimateTransferGasUseCase.Param): Single<EthEstimateGas>

    fun getCap(param: GetCombinedCapUseCase.Param): Single<Cap>
}