package com.nichie.curdex.domain.usecase.profile

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.LoginUser
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val userRepository: UserRepository
) : SequentialUseCase<LoginUseCase.Param, LoginUser>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseSingle(param: Param): Single<LoginUser> {
        return userRepository.login(param)
    }

    class Param(
        val email: String,
        val password: String,
        val twoFa: String?
    )
}
