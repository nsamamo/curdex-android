package com.nichie.curdex.domain.usecase.swap

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.NotificationAlert
import com.nichie.curdex.domain.model.Swap
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetSwapDataUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val swapRepository: SwapRepository
) : FlowableUseCase<GetSwapDataUseCase.Param, Swap>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<Swap> {
        return swapRepository.getSwapData(param)
    }

    class Param(val wallet: Wallet, val alert: NotificationAlert? = null)
}
