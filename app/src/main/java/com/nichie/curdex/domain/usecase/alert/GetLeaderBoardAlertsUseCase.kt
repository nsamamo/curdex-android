package com.nichie.curdex.domain.usecase.alert

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.LeaderBoard
import com.nichie.curdex.domain.repository.AlertRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetLeaderBoardAlertsUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val alertRepository: AlertRepository
) : SequentialUseCase<String?, LeaderBoard>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: String?): Single<LeaderBoard> {
        return alertRepository.getLeaderBoardAlert()
    }
}
