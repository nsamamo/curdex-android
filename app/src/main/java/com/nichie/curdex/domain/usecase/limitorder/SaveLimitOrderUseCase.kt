package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.LocalLimitOrder
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class SaveLimitOrderUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : CompletableUseCase<SaveLimitOrderUseCase.Param, Any?>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseCompletable(param: Param): Completable {
        return limitOrderRepository.saveLimitOrder(param)
    }

    class Param(val order: LocalLimitOrder)
}
