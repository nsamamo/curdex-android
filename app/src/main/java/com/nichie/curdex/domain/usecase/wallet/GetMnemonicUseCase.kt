package com.nichie.curdex.domain.usecase.wallet

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Word
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetMnemonicUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val walletRepository: WalletRepository
) : SequentialUseCase<GetMnemonicUseCase.Param, List<Word>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseSingle(param: Param): Single<List<Word>> {
        return walletRepository.getMnemonic(param)
    }

    class Param(
        val password: String,
        val walletId: String
    )
}
