package com.nichie.curdex.domain.usecase.transaction

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Transaction
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.TransactionRepository
import com.nichie.curdex.domain.usecase.MergeDelayErrorUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetTransactionsUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val transactionRepository: TransactionRepository
) : MergeDelayErrorUseCase<GetTransactionsUseCase.Param, TransactionsData>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<TransactionsData> {
        return transactionRepository.fetchAllTransactions(param)
    }

    class Param(
        val wallet: Wallet,
        val isForceRefresh: Boolean = false
    )
}

data class TransactionsData(val transactionList: List<Transaction>, val isLoaded: Boolean)

