package com.nichie.curdex.domain.usecase.transaction

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Transaction
import com.nichie.curdex.domain.repository.TransactionRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetPendingTransactionsUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val transactionRepository: TransactionRepository
) : FlowableUseCase<String, List<Transaction>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: String): Flowable<List<Transaction>> {
        return transactionRepository.fetchPendingTransaction(param)
    }
}
