package com.nichie.curdex.domain.model

class WalletChangeEvent(val walletAddress: String)