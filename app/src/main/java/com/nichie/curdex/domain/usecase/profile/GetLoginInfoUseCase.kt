package com.nichie.curdex.domain.usecase.profile

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.UserInfo
import com.nichie.curdex.domain.repository.UserRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetLoginInfoUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val userRepository: UserRepository
) : FlowableUseCase<String?, UserInfo>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: String?): Flowable<UserInfo> {
        return userRepository.getUser()
    }
}
