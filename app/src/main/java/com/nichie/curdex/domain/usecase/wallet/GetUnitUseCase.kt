package com.nichie.curdex.domain.usecase.wallet

import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetUnitUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val walletRepository: WalletRepository
) : FlowableUseCase<String?, String>(schedulerProvider) {
    override fun buildUseCaseFlowable(param: String?): Flowable<String> {
        return walletRepository.getSelectedUnit()
    }
}
