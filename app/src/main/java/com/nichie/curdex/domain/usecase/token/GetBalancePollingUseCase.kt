package com.nichie.curdex.domain.usecase.token

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.BalanceRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetBalancePollingUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val balanceRepository: BalanceRepository
) : FlowableUseCase<GetBalancePollingUseCase.Param, List<Token>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<List<Token>> {
        return balanceRepository.getChange24hPolling(param)
    }

    class Param(val wallets: List<Wallet>)
}
