package com.nichie.curdex.domain.usecase.transaction

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Transaction
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.TransactionRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class MonitorPendingTransactionUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val transactionRepository: TransactionRepository
) : FlowableUseCase<MonitorPendingTransactionUseCase.Param, List<Transaction>>(schedulerProvider) {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<List<Transaction>> {
        return transactionRepository.monitorPendingTransactionsPolling(param)
    }

    class Param(val transactions: List<Transaction>, val wallet: Wallet)
}
