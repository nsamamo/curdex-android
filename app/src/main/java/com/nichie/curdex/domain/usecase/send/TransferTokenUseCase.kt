package com.nichie.curdex.domain.usecase.send

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.ResponseStatus
import com.nichie.curdex.domain.model.Send
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.SwapRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class TransferTokenUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val swapRepository: SwapRepository
) : SequentialUseCase<TransferTokenUseCase.Param, ResponseStatus>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseSingle(param: Param): Single<ResponseStatus> {
        return swapRepository.transferToken(param)
    }

    class Param(val wallet: Wallet, val send: Send)
}
