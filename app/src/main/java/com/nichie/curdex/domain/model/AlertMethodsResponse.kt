package com.nichie.curdex.domain.model


import com.google.gson.annotations.SerializedName
import com.nichie.curdex.data.api.alert.AlertMethodsResponseEntity

data class AlertMethodsResponse(
    @SerializedName("success")
    val success: Boolean = false,
    @SerializedName("data")
    val `data`: AlertMethods = AlertMethods()
) {
    constructor(entity: AlertMethodsResponseEntity) : this(
        entity.success,
        AlertMethods(entity.data)
    )
}