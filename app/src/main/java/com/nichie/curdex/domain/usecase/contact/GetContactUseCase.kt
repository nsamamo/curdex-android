package com.nichie.curdex.domain.usecase.contact

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.repository.ContactRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetContactUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val contactRepository: ContactRepository
) : FlowableUseCase<GetContactUseCase.Param, List<Contact>>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<List<Contact>> {
        return contactRepository.getContacts(param)
    }

    class Param
}
