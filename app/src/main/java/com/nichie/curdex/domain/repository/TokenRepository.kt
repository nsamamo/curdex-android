package com.nichie.curdex.domain.repository

import com.nichie.curdex.domain.model.Chart
import com.nichie.curdex.domain.usecase.swap.GetExpectedRateUseCase
import com.nichie.curdex.domain.usecase.swap.GetMarketRateUseCase
import com.nichie.curdex.domain.usecase.token.GetChartDataForTokenUseCase
import com.nichie.curdex.domain.usecase.token.SaveTokenUseCase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface TokenRepository {
    fun getExpectedRate(param: GetExpectedRateUseCase.Param): Flowable<List<String>>

    fun getMarketRate(param: GetMarketRateUseCase.Param): Flowable<String>

    fun getChartData(param: GetChartDataForTokenUseCase.Param): Single<Chart>

    fun saveToken(param: SaveTokenUseCase.Param): Completable

}