package com.nichie.curdex.domain.usecase.limitorder

import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.LocalLimitOrder
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.LimitOrderRepository
import com.nichie.curdex.domain.usecase.FlowableUseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetLocalLimitOrderDataUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val limitOrderRepository: LimitOrderRepository
) : FlowableUseCase<GetLocalLimitOrderDataUseCase.Param, LocalLimitOrder>(schedulerProvider) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    override fun buildUseCaseFlowable(param: Param): Flowable<LocalLimitOrder> {
        return limitOrderRepository.getCurrentLimitOrders(param)
    }

    class Param(val wallet: Wallet)
}
