package com.nichie.curdex.domain.usecase.wallet

import android.net.Uri
import androidx.annotation.VisibleForTesting
import com.nichie.curdex.domain.SchedulerProvider
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.repository.WalletRepository
import com.nichie.curdex.domain.usecase.SequentialUseCase
import io.reactivex.Single
import javax.inject.Inject

class ImportWalletFromJsonUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val walletRepository: WalletRepository
) : SequentialUseCase<ImportWalletFromJsonUseCase.Param, Pair<Wallet, List<Token>>>(
    schedulerProvider
) {
    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public override fun buildUseCaseSingle(param: Param): Single<Pair<Wallet, List<Token>>> {
        return walletRepository.importWallet(param)
    }

    class Param(val uri: Uri, val password: String, val walletName: String)
}
