package com.nichie.curdex.domain.model

import com.nichie.curdex.data.api.limitorder.CancelledEntity


data class Cancelled(
    val cancelled: Boolean = false
) {
    constructor(entity: CancelledEntity) : this(entity.cancelled)
}