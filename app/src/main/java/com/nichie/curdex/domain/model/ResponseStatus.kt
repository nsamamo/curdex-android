package com.nichie.curdex.domain.model

import com.nichie.curdex.data.api.user.ResponseStatusEntity


data class ResponseStatus(
    val message: String = "",
    val success: Boolean = false
) {
    constructor(entity: ResponseStatusEntity) : this(
        entity.message, entity.success
    )
}