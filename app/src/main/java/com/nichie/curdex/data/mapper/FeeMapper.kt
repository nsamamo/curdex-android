package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.limitorder.FeeEntity
import com.nichie.curdex.domain.model.Fee
import javax.inject.Inject

class FeeMapper @Inject constructor() {
    fun transform(entity: FeeEntity): Fee {
        return Fee(entity)
    }
}