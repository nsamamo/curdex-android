package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.limitorder.*
import com.nichie.curdex.domain.model.*
import javax.inject.Inject

class OrderMapper @Inject constructor() {
    fun transform(entity: OrderEntity): Order {
        return Order(entity)
    }

    fun transform(entities: List<OrderEntity>): List<Order> {
        return entities.map {
            transform(it)
        }
    }

    fun transform(entity: LimitOrderResponseEntity): LimitOrderResponse {
        return LimitOrderResponse(entity)
    }

    fun transform(entity: CancelledEntity): Cancelled {
        return Cancelled(entity)
    }

    fun transform(entity: PendingBalancesEntity): PendingBalances {
        return PendingBalances(entity)
    }

    fun transform(entity: EligibleAddressEntity): EligibleAddress {
        return EligibleAddress(entity)
    }

}