package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.chart.ChartResponseEntity
import com.nichie.curdex.domain.model.Chart
import javax.inject.Inject

class ChartMapper @Inject constructor() {
    fun transform(chartResponseEntity: ChartResponseEntity): Chart {

        return Chart(chartResponseEntity)
    }
}