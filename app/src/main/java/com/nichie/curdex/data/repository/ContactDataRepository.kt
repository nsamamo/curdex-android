package com.nichie.curdex.data.repository

import com.nichie.curdex.data.db.ContactDao
import com.nichie.curdex.data.db.SendDao
import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.repository.ContactRepository
import com.nichie.curdex.domain.usecase.contact.DeleteContactUseCase
import com.nichie.curdex.domain.usecase.contact.GetContactUseCase
import com.nichie.curdex.domain.usecase.contact.SaveContactUseCase
import com.nichie.curdex.presentation.common.DEFAULT_NAME
import io.reactivex.Completable
import io.reactivex.Flowable
import java.util.Locale
import javax.inject.Inject


class ContactDataRepository @Inject constructor(
    private val contactDao: ContactDao,
    private val sendDao: SendDao

) : ContactRepository {

    override fun saveContact(param: SaveContactUseCase.Param): Completable {
        return Completable.fromCallable {
            val name = if (param.name.isEmpty()) {
                DEFAULT_NAME
            } else param.name

            val findContactByAddress = contactDao.findContactByAddress(param.address)
            val updatedAt = System.currentTimeMillis() / 1000
            val contact = findContactByAddress?.copy(
                address = param.address.toLowerCase(Locale.getDefault()),
                name = name,
                updatedAt = updatedAt
            ) ?: Contact(param.walletAddress, param.address.toLowerCase(Locale.getDefault()), name, updatedAt)
            contactDao.insertContact(contact)

            if (param.isSend) {
                val send = sendDao.findSendByAddress(param.walletAddress)
                send?.let {
                    sendDao.updateSend(it.copy(contact = contact))
                }
            }

        }
    }

    override fun getContacts(param: GetContactUseCase.Param): Flowable<List<Contact>> {
        return contactDao.all.map { contacts ->
            contacts.sortedByDescending { it.updatedAt }
        }
    }

    override fun deleteContact(param: DeleteContactUseCase.Param): Completable {
        return contactDao.deleteContactCompletable(param.contact)
    }

}