package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.alert.AlertEntity
import com.nichie.curdex.data.api.alert.AlertMethodsResponseEntity
import com.nichie.curdex.data.api.user.KycResponseStatusEntity
import com.nichie.curdex.data.api.user.LoginUserEntity
import com.nichie.curdex.data.api.user.ResponseStatusEntity
import com.nichie.curdex.data.api.user.UserInfoEntity
import com.nichie.curdex.domain.model.*
import javax.inject.Inject

class UserMapper @Inject constructor() {
    fun transform(entity: ResponseStatusEntity): ResponseStatus {
        return ResponseStatus(entity)
    }

    fun transform(entity: LoginUserEntity): LoginUser {
        return LoginUser(entity)
    }

    fun transform(entity: UserInfoEntity): UserInfo {
        return UserInfo(entity)
    }

    fun transform(entity: KycResponseStatusEntity): KycResponseStatus {
        return KycResponseStatus(entity)
    }

    fun transform(alerts: List<AlertEntity>): List<Alert> {
        return alerts.map {
            Alert(it)
        }
    }

    fun transform(entity: AlertMethodsResponseEntity): AlertMethodsResponse {
        return AlertMethodsResponse(entity)
    }
}