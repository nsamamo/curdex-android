package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.cap.CapEntity
import com.nichie.curdex.domain.model.Cap
import javax.inject.Inject

class CapMapper @Inject constructor() {
    fun transform(entity: CapEntity): Cap {
        return Cap(entity)
    }
}