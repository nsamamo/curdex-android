package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.rate.RateEntity
import com.nichie.curdex.domain.model.Rate
import javax.inject.Inject

class RateMapper @Inject constructor() {
    fun transform(entity: RateEntity): Rate {
        return Rate(entity)
    }

    fun transform(entities: List<RateEntity>): List<Rate> {
        return entities.map {
            transform(it)
        }
    }
}