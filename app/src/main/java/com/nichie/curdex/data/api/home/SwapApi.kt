package com.nichie.curdex.data.api.home

import com.nichie.curdex.data.api.cap.CapEntity
import com.nichie.curdex.data.api.gas.GasPriceEntity
import com.nichie.curdex.data.api.rate.MarketRateEntity
import com.nichie.curdex.domain.model.EstimateAmount
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SwapApi {
    @GET("rate")
    fun getRate(): Single<MarketRateEntity>

    @GET("gasPrice")
    fun getGasPrice(): Single<GasPriceEntity>

    @GET("users")
    fun getCap(
        @Query("address") address: String?
    ): Single<CapEntity>

    @GET("sourceAmount")
    fun sourceAmount(
        @Query("source") source: String,
        @Query("dest") dest: String,
        @Query("destAmount") destAmount: String
    ): Single<EstimateAmount>
}