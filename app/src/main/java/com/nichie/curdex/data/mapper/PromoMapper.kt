package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.promo.PromoResponseEntity
import com.nichie.curdex.domain.model.Promo
import javax.inject.Inject

class PromoMapper @Inject constructor() {
    fun transform(promoEntity: PromoResponseEntity): Promo {
        promoEntity.data.error = promoEntity.error
        return Promo(promoEntity.data)
    }

}