package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.alert.AlertEntity
import com.nichie.curdex.data.api.alert.LeaderBoardEntity
import com.nichie.curdex.data.api.user.ResponseStatusEntity
import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.model.LeaderBoard
import com.nichie.curdex.domain.model.ResponseStatus
import javax.inject.Inject

class AlertMapper @Inject constructor() {
    fun transform(alertEntity: AlertEntity): Alert {
        return Alert(alertEntity)
    }

    fun transform(alertEntity: ResponseStatusEntity): ResponseStatus {
        return ResponseStatus(alertEntity)
    }


    fun transform(entity: LeaderBoardEntity): LeaderBoard {
        return LeaderBoard(entity)
    }
}