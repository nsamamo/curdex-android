package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.gas.GasEntity
import com.nichie.curdex.domain.model.Gas
import javax.inject.Inject

class GasMapper @Inject constructor() {
    fun transform(entity: GasEntity): Gas {
        return Gas(entity)
    }
}