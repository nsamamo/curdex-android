package com.nichie.curdex.data.mapper

import com.nichie.curdex.data.api.token.TokenEntity
import com.nichie.curdex.domain.model.Token
import javax.inject.Inject

class TokenMapper @Inject constructor() {
    fun transform(entity: TokenEntity): Token {
        return Token(entity)
    }
}