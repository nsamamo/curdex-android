package com.nichie.curdex.data.api.limitorder


import com.google.gson.annotations.SerializedName

data class CancelledEntity(
    @SerializedName("cancelled")
    val cancelled: Boolean = false
)