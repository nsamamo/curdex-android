package com.nichie.curdex.data.api.home

import com.nichie.curdex.data.api.currencies.CurrencyEntity
import io.reactivex.Single
import retrofit2.http.GET

interface CurrencyApi {
    @GET("internal/currencies")
    fun internalCurrencies(): Single<CurrencyEntity>
}