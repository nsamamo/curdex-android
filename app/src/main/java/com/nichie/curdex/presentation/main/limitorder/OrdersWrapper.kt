package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.Order


data class OrdersWrapper(val orders: List<Order>, val asc: Boolean)