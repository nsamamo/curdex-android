package com.nichie.curdex.presentation.main.balance.send

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.model.Send
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.usecase.contact.DeleteContactUseCase
import com.nichie.curdex.domain.usecase.contact.GetContactUseCase
import com.nichie.curdex.domain.usecase.contact.SaveContactUseCase
import com.nichie.curdex.domain.usecase.send.GetSendTokenUseCase
import com.nichie.curdex.domain.usecase.send.SaveSendUseCase
import com.nichie.curdex.domain.usecase.swap.EstimateTransferGasUseCase
import com.nichie.curdex.domain.usecase.swap.GetGasPriceUseCase
import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.common.ADDITIONAL_SEND_GAS_LIMIT
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.common.calculateDefaultGasLimitTransfer
import com.nichie.curdex.presentation.common.specialGasLimitDefault
import com.nichie.curdex.presentation.main.SelectedWalletViewModel
import com.nichie.curdex.presentation.main.swap.DeleteContactState
import com.nichie.curdex.presentation.main.swap.GetContactState
import com.nichie.curdex.presentation.main.swap.GetGasLimitState
import com.nichie.curdex.presentation.main.swap.GetGasPriceState
import com.nichie.curdex.presentation.main.swap.GetSendState
import com.nichie.curdex.presentation.main.swap.SaveContactState
import com.nichie.curdex.presentation.main.swap.SaveSendState
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import java.math.BigInteger
import javax.inject.Inject

class SendViewModel @Inject constructor(
    private val saveContactUseCase: SaveContactUseCase,
    private val getSendTokenUseCase: GetSendTokenUseCase,
    private val getGasPriceUseCase: GetGasPriceUseCase,
    private val saveSendUseCase: SaveSendUseCase,
    private val getContactUseCase: GetContactUseCase,
    private val deleteContactUseCase: DeleteContactUseCase,
    private val estimateTransferGasUseCase: EstimateTransferGasUseCase,
    getSelectedWalletUseCase: GetSelectedWalletUseCase,
    private val errorHandler: ErrorHandler
) : SelectedWalletViewModel(getSelectedWalletUseCase, errorHandler) {
    val compositeDisposable = CompositeDisposable()
    private val _getGetGasPriceCallback = MutableLiveData<Event<GetGasPriceState>>()
    val getGetGasPriceCallback: LiveData<Event<GetGasPriceState>>
        get() = _getGetGasPriceCallback

    private val _getSendCallback = MutableLiveData<Event<GetSendState>>()
    val getSendCallback: LiveData<Event<GetSendState>>
        get() = _getSendCallback

    private val _getContactCallback = MutableLiveData<Event<GetContactState>>()
    val getContactCallback: LiveData<Event<GetContactState>>
        get() = _getContactCallback

    private val _saveSendCallback = MutableLiveData<Event<SaveSendState>>()
    val saveSendCallback: LiveData<Event<SaveSendState>>
        get() = _saveSendCallback

    private val _getGetGasLimitCallback = MutableLiveData<Event<GetGasLimitState>>()
    val getGetGasLimitCallback: LiveData<Event<GetGasLimitState>>
        get() = _getGetGasLimitCallback

    private val _saveContactCallback = MutableLiveData<Event<SaveContactState>>()
    val saveContactCallback: LiveData<Event<SaveContactState>>
        get() = _saveContactCallback

    private val _deleteContactCallback = MutableLiveData<Event<DeleteContactState>>()
    val deleteContactCallback: LiveData<Event<DeleteContactState>>
        get() = _deleteContactCallback

    val currentContactSelection: LiveData<Event<Contact>>
        get() = _currentSelection

    private val _currentSelection = MutableLiveData<Event<Contact>>()

    fun updateCurrentContact(contact: Contact) {
        _currentSelection.value = Event(contact)
    }

    fun getSendInfo(wallet: Wallet) {
        getSendTokenUseCase.dispose()
        getSendTokenUseCase.execute(
            Consumer {
                it.gasLimit =
                    calculateGasLimit(it).toString()
                _getSendCallback.value = Event(GetSendState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getSendCallback.value = Event(GetSendState.ShowError(errorHandler.getError(it)))
            },
            GetSendTokenUseCase.Param(wallet)
        )
    }

    private fun calculateGasLimit(send: Send): BigInteger {
        return calculateDefaultGasLimitTransfer(send.tokenSource)
    }


    fun deleteContact(contact: Contact) {
        deleteContactUseCase.execute(
            Action {
                _deleteContactCallback.value = Event(DeleteContactState.Success(""))
            },
            Consumer {
                it.printStackTrace()
                _deleteContactCallback.value =
                    Event(DeleteContactState.ShowError(errorHandler.getError(it)))
            },
            DeleteContactUseCase.Param(contact)
        )
    }


    fun saveSendContact(walletAddress: String, contact: Contact) {
        saveContactUseCase.execute(
            Action {
                _saveContactCallback.value = Event(SaveContactState.Success())
            },
            Consumer {
                it.printStackTrace()
                _saveContactCallback.value =
                    Event(SaveContactState.ShowError(errorHandler.getError(it)))
            },
            SaveContactUseCase.Param(walletAddress, contact.address, contact.name)
        )
    }


    fun getGasPrice() {
        getGasPriceUseCase.dispose()
        getGasPriceUseCase.execute(
            Consumer {
                _getGetGasPriceCallback.value = Event(GetGasPriceState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getGetGasPriceCallback.value =
                    Event(GetGasPriceState.ShowError(errorHandler.getError(it)))
            },
            null
        )
    }

    fun saveSend(send: Send?, address: String = "") {
        send?.let {
            saveSendUseCase.execute(
                Action {
                    if (address.isNotEmpty()) {
                        _saveSendCallback.value = Event(SaveSendState.Success(""))
                    }
                },
                Consumer { error ->
                    error.printStackTrace()
                    _saveSendCallback.value =
                        Event(SaveSendState.ShowError(errorHandler.getError(error)))
                },
                SaveSendUseCase.Param(it, address)
            )
        }
    }

    fun getContact() {
        getContactUseCase.execute(
            Consumer {
                _getContactCallback.value = Event(GetContactState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getContactCallback.value =
                    Event(GetContactState.ShowError(errorHandler.getError(it)))
            },
            GetContactUseCase.Param()
        )
    }

    fun getGasLimit(send: Send?, wallet: Wallet?) {
        if (send == null || wallet == null) return
        estimateTransferGasUseCase.dispose()
        estimateTransferGasUseCase.execute(
            Consumer {

                val gasLimit = calculateDefaultGasLimitTransfer(send.tokenSource)
                    .min(it.amountUsed.multiply(120.toBigInteger()).divide(100.toBigInteger()) + ADDITIONAL_SEND_GAS_LIMIT.toBigInteger())

                val specialGasLimit = specialGasLimitDefault(send.tokenSource, send.tokenSource)

                _getGetGasLimitCallback.value = Event(
                    GetGasLimitState.Success(
                        if (specialGasLimit != null) {
                            specialGasLimit.min(gasLimit)
                        } else {
                            gasLimit
                        }
                    )
                )
            },
            Consumer {
                it.printStackTrace()
            },
            EstimateTransferGasUseCase.Param(wallet, send)
        )
    }

    override fun onCleared() {
        getSendTokenUseCase.dispose()
        getGasPriceUseCase.dispose()
        saveSendUseCase.dispose()
        getContactUseCase.dispose()
        estimateTransferGasUseCase.dispose()
        super.onCleared()
    }
}