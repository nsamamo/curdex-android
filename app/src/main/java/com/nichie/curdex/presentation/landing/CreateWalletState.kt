package com.nichie.curdex.presentation.landing

import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.model.Word


sealed class CreateWalletState {
    object Loading : CreateWalletState()
    class ShowError(val message: String?) : CreateWalletState()
    class Success(val wallet: Wallet, val words: List<Word>) : CreateWalletState()
}
