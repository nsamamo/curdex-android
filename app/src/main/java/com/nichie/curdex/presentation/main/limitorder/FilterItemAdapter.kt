package com.nichie.curdex.presentation.main.limitorder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.nichie.curdex.AppExecutors
import com.nichie.curdex.R
import com.nichie.curdex.databinding.ItemFilterBinding
import com.nichie.curdex.domain.model.FilterItem
import com.nichie.curdex.presentation.base.DataBoundListAdapter

class FilterItemAdapter(
    appExecutors: AppExecutors,
    private val onCancelClick: ((FilterItem) -> Unit)?

) : DataBoundListAdapter<FilterItem, ItemFilterBinding>(
    appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<FilterItem>() {
        override fun areItemsTheSame(oldItem: FilterItem, newItem: FilterItem): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: FilterItem, newItem: FilterItem): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun bind(binding: ItemFilterBinding, item: FilterItem) {
        binding.cb.setOnCheckedChangeListener { _, isChecked ->
            item.isSelected = isChecked
        }
        binding.name = item.itemName
        binding.isSelected = item.isSelected
        binding.executePendingBindings()
    }


    fun resetFilter() {
        submitList(getData().map {
            it.isSelected = true
            it
        })
        notifyDataSetChanged()
    }


    override fun createBinding(parent: ViewGroup, viewType: Int): ItemFilterBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_filter,
            parent,
            false
        )
}