package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.PendingBalances

sealed class GetPendingBalancesState {
    object Loading : GetPendingBalancesState()
    class ShowError(val message: String?) : GetPendingBalancesState()
    class Success(val pendingBalances: PendingBalances) : GetPendingBalancesState()
}
