package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.Contact

sealed class GetContactState {
    object Loading : GetContactState()
    class ShowError(val message: String?) : GetContactState()
    class Success(val contacts: List<Contact>) : GetContactState()
}
