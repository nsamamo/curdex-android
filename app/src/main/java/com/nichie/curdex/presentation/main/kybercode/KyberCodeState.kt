package com.nichie.curdex.presentation.main.kybercode

import com.nichie.curdex.domain.model.Wallet

sealed class KyberCodeState {
    object Loading : KyberCodeState()
    class ShowError(val message: String?) : KyberCodeState()
    class Success(val wallet: Wallet) : KyberCodeState()
}
