package com.nichie.curdex.presentation.main.setting.wallet

import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.main.SelectedWalletViewModel
import com.nichie.curdex.util.ErrorHandler
import javax.inject.Inject

class BackupWalletInfoViewModel @Inject constructor(
    getSelectedWalletUseCase: GetSelectedWalletUseCase,
    errorHandler: ErrorHandler
) : SelectedWalletViewModel(getSelectedWalletUseCase, errorHandler)