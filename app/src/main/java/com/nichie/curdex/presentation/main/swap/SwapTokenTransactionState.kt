package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.ResponseStatus

sealed class SwapTokenTransactionState {
    object Loading : SwapTokenTransactionState()
    class ShowError(val message: String?) : SwapTokenTransactionState()
    class Success(val responseStatus: ResponseStatus? = null) : SwapTokenTransactionState()
}
