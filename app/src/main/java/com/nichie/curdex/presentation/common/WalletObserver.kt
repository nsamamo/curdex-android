package com.nichie.curdex.presentation.common

interface WalletObserver {

    fun refresh()
}