package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.Fee

sealed class GetFeeState {
    object Loading : GetFeeState()
    class ShowError(val message: String?, val isNetworkUnAvailable: Boolean = false) : GetFeeState()
    class Success(val fee: Fee) : GetFeeState()
}
