package com.nichie.curdex.presentation.main.setting

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nichie.curdex.domain.model.Contact
import com.nichie.curdex.domain.usecase.contact.DeleteContactUseCase
import com.nichie.curdex.domain.usecase.contact.GetContactUseCase
import com.nichie.curdex.domain.usecase.contact.SaveContactUseCase
import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.main.SelectedWalletViewModel
import com.nichie.curdex.presentation.main.swap.DeleteContactState
import com.nichie.curdex.presentation.main.swap.GetContactState
import com.nichie.curdex.presentation.main.swap.SaveContactState
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ContactViewModel @Inject constructor(
    private val saveContactUseCase: SaveContactUseCase,
    private val getContactUseCase: GetContactUseCase,
    private val deleteContactUseCase: DeleteContactUseCase,
    getSelectedWalletUseCase: GetSelectedWalletUseCase,
    errorHandler: ErrorHandler
) : SelectedWalletViewModel(getSelectedWalletUseCase, errorHandler) {
    private val _saveContactCallback = MutableLiveData<Event<SaveContactState>>()
    val saveContactCallback: LiveData<Event<SaveContactState>>
        get() = _saveContactCallback

    private val _getContactCallback = MutableLiveData<Event<GetContactState>>()
    val getContactCallback: LiveData<Event<GetContactState>>
        get() = _getContactCallback

    private val _deleteContactCallback = MutableLiveData<Event<DeleteContactState>>()
    val deleteContactCallback: LiveData<Event<DeleteContactState>>
        get() = _deleteContactCallback


    fun saveSendContact(walletAddress: String, contact: Contact) {
        saveContactUseCase.execute(
            Action {
                _saveContactCallback.value = Event(SaveContactState.Success())
            },
            Consumer {
                it.printStackTrace()
                _saveContactCallback.value =
                    Event(SaveContactState.ShowError(it.localizedMessage))
            },
            SaveContactUseCase.Param(walletAddress, contact.address, contact.name, true)
        )
    }

    fun getContact() {
        getContactUseCase.execute(
            Consumer {
                _getContactCallback.value = Event(GetContactState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getContactCallback.value = Event(GetContactState.ShowError(it.localizedMessage))
            },
            GetContactUseCase.Param()
        )
    }

    fun deleteContact(contact: Contact) {
        deleteContactUseCase.execute(
            Action {
                _deleteContactCallback.value = Event(DeleteContactState.Success(""))
            },
            Consumer {
                it.printStackTrace()
                _deleteContactCallback.value =
                    Event(DeleteContactState.ShowError(it.localizedMessage))
            },
            DeleteContactUseCase.Param(contact)
        )
    }

    override fun onCleared() {
        saveContactUseCase.dispose()
        getContactUseCase.dispose()
        super.onCleared()
    }

}