package com.nichie.curdex.presentation.common

interface PendingTransactionNotification {

    fun showNotification(showNotification: Boolean)
}