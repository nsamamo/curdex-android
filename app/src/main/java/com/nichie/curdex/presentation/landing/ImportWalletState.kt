package com.nichie.curdex.presentation.landing

import com.nichie.curdex.domain.model.Wallet


sealed class ImportWalletState {
    object Loading : ImportWalletState()
    class ShowError(val message: String?) : ImportWalletState()
    class Success(val wallet: Wallet) : ImportWalletState()
}
