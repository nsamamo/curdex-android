package com.nichie.curdex.presentation.main.profile.alert

import com.nichie.curdex.domain.model.ResponseStatus

sealed class UpdateAlertMethodsState {
    object Loading : UpdateAlertMethodsState()
    class ShowError(val message: String?) : UpdateAlertMethodsState()
    class Success(val status: ResponseStatus) : UpdateAlertMethodsState()
}
