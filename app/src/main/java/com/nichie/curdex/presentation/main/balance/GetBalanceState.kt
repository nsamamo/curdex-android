package com.nichie.curdex.presentation.main.balance

import com.nichie.curdex.domain.model.PendingBalances
import com.nichie.curdex.domain.model.Token

sealed class GetBalanceState {
    object Loading : GetBalanceState()
    class ShowError(val message: String?) : GetBalanceState()
    class Success(val tokens: List<Token>, val pendingBalances: PendingBalances? = null) :
        GetBalanceState()
}
