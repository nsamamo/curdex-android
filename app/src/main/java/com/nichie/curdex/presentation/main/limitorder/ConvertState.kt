package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.LocalLimitOrder

sealed class ConvertState {
    object Loading : ConvertState()
    class ShowError(val message: String?) : ConvertState()
    class Success(val limitOrder: LocalLimitOrder) : ConvertState()
}
