package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.Cancelled

sealed class CancelOrdersState {
    object Loading : CancelOrdersState()
    class ShowError(val message: String?) : CancelOrdersState()
    class Success(val cancelled: Cancelled) : CancelOrdersState()
}
