package com.nichie.curdex.presentation.setting

import com.nichie.curdex.domain.model.VerifyStatus

sealed class VerifyPinState {
    object Loading : VerifyPinState()
    class ShowError(val message: String?) : VerifyPinState()
    class Success(val verifyStatus: VerifyStatus) : VerifyPinState()
}
