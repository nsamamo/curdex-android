package com.nichie.curdex.presentation.splash

import com.nichie.curdex.domain.model.Wallet

sealed class GetWalletState {
    object Loading : GetWalletState()
    class ShowError(val message: String?) : GetWalletState()
    class Success(val wallet: Wallet) : GetWalletState()
}
