package com.nichie.curdex.presentation.main.alert

import com.nichie.curdex.domain.model.Alert

sealed class GetAlertState {
    object Loading : GetAlertState()
    class ShowError(val message: String?) : GetAlertState()
    class Success(val alert: Alert) : GetAlertState()
}
