package com.nichie.curdex.presentation.main.profile.alert

import com.nichie.curdex.domain.model.Alert

sealed class GetAlertsState {
    object Loading : GetAlertsState()
    class ShowError(val message: String?) : GetAlertsState()
    class Success(val alerts: List<Alert>) : GetAlertsState()
}
