package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.Order

sealed class SubmitOrderState {
    object Loading : SubmitOrderState()
    class ShowError(val message: String?) : SubmitOrderState()
    class Success(val order: Order) : SubmitOrderState()
}
