package com.nichie.curdex.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.splash.GetWalletState
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Consumer
import javax.inject.Inject

open class SelectedWalletViewModel @Inject constructor(
    private val getWalletUseCase: GetSelectedWalletUseCase,
    private val errorHandler: ErrorHandler
) : ViewModel() {

    private val _getSelectedWalletCallback = MutableLiveData<Event<GetWalletState>>()
    val getSelectedWalletCallback: LiveData<Event<GetWalletState>>
        get() = _getSelectedWalletCallback

    fun getSelectedWallet() {
        getWalletUseCase.dispose()
        getWalletUseCase.execute(
            Consumer { wallet ->
                _getSelectedWalletCallback.value = Event(GetWalletState.Success(wallet))

            },
            Consumer {
                it.printStackTrace()
                _getSelectedWalletCallback.value =
                    Event(GetWalletState.ShowError(errorHandler.getError(it)))
            },
            null
        )
    }

    override fun onCleared() {
        getWalletUseCase.dispose()
        super.onCleared()
    }
}