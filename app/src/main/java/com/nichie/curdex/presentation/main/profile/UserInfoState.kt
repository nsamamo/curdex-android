package com.nichie.curdex.presentation.main.profile

import com.nichie.curdex.domain.model.UserInfo

sealed class UserInfoState {
    object Loading : UserInfoState()
    class ShowError(val message: String?) : UserInfoState()
    class Success(val userInfo: UserInfo?) : UserInfoState()
}
