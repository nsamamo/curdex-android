package com.nichie.curdex.presentation.main.transaction

import com.nichie.curdex.domain.model.FilterItem
import com.nichie.curdex.domain.model.TransactionFilter

sealed class GetTransactionFilterState {
    object Loading : GetTransactionFilterState()
    class ShowError(val message: String?) : GetTransactionFilterState()
    class Success(val transactionFilter: TransactionFilter, val tokens: List<FilterItem>) :
        GetTransactionFilterState()
}
