package com.nichie.curdex.presentation.main.alert

import com.nichie.curdex.domain.model.Alert

sealed class GetCurrentAlertState {
    object Loading : GetCurrentAlertState()
    class ShowError(val message: String?) : GetCurrentAlertState()
    class Success(val alert: Alert) : GetCurrentAlertState()
}
