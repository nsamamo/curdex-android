package com.nichie.curdex.presentation.main.balance.chart

import com.nichie.curdex.domain.model.Chart

sealed class GetChartState {
    object Loading : GetChartState()
    class ShowError(val message: String?) : GetChartState()
    class Success(val chart: Chart) : GetChartState()
}
