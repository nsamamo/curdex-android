package com.nichie.curdex.presentation.main.profile

import com.nichie.curdex.domain.model.LoginUser
import com.nichie.curdex.domain.model.SocialInfo

sealed class LoginState {
    object Loading : LoginState()
    class ShowError(val message: String?) : LoginState()
    class Success(val login: LoginUser, val socialInfo: SocialInfo) : LoginState()
}
