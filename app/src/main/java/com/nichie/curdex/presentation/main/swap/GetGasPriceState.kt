package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.Gas

sealed class GetGasPriceState {
    object Loading : GetGasPriceState()
    class ShowError(val message: String?) : GetGasPriceState()
    class Success(val gas: Gas) : GetGasPriceState()
}
