package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.Swap

sealed class GetSwapState {
    object Loading : GetSwapState()
    class ShowError(val message: String?) : GetSwapState()
    class Success(val swap: Swap) : GetSwapState()
}
