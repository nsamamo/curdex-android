package com.nichie.curdex.presentation.main.setting.wallet

import com.nichie.curdex.domain.model.VerifyStatus

sealed class DeleteWalletState {
    object Loading : DeleteWalletState()
    class ShowError(val message: String?) : DeleteWalletState()
    class Success(val verifyStatus: VerifyStatus) : DeleteWalletState()
}
