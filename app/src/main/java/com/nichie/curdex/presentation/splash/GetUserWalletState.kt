package com.nichie.curdex.presentation.splash

import com.nichie.curdex.domain.model.Wallet

sealed class GetUserWalletState {
    object Loading : GetUserWalletState()
    class ShowError(val message: String?) : GetUserWalletState()
    class Success(val wallet: Wallet) : GetUserWalletState()
}
