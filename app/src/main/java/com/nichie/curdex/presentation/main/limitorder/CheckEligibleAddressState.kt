package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.EligibleAddress

sealed class CheckEligibleAddressState {
    object Loading : CheckEligibleAddressState()
    class ShowError(val message: String?, val isNetworkUnavailable: Boolean = false) :
        CheckEligibleAddressState()

    class Success(val eligibleAddress: EligibleAddress, val isWalletChangeEvent: Boolean = false) :
        CheckEligibleAddressState()
}
