package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.Send

sealed class GetSendState {
    object Loading : GetSendState()
    class ShowError(val message: String?) : GetSendState()
    class Success(val send: Send) : GetSendState()
}
