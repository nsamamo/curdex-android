package com.nichie.curdex.presentation.wallet

import com.nichie.curdex.domain.usecase.token.GetTokenBalanceUseCase
import com.nichie.curdex.domain.usecase.wallet.ImportWalletFromPrivateKeyUseCase
import com.nichie.curdex.presentation.landing.ImportWalletState
import io.reactivex.functions.Consumer
import org.consenlabs.tokencore.wallet.model.TokenException
import javax.inject.Inject

class ImportPrivateKeyViewModel @Inject constructor(
    private val importWalletFromPrivateKeyUseCase: ImportWalletFromPrivateKeyUseCase,
    getTokenBalance: GetTokenBalanceUseCase
) : ImportWalletViewModel(getTokenBalance) {


    fun importFromPrivateKey(privateKey: String, walletName: String) {
        importWalletCallback.postValue(ImportWalletState.Loading)
        importWalletFromPrivateKeyUseCase.execute(
            Consumer {
                loadBalances(it)
            },
            Consumer {
                it.printStackTrace()
                if (it is TokenException) {
                    importWalletCallback.value = ImportWalletState.ShowError(it.message)
                } else {
                    importWalletCallback.value = ImportWalletState.ShowError(it.localizedMessage)
                }
            },
            ImportWalletFromPrivateKeyUseCase.Param(privateKey, walletName)
        )
    }
}