package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.Cap
import com.nichie.curdex.domain.model.Swap

sealed class GetCapState {
    object Loading : GetCapState()
    class ShowError(val message: String?) : GetCapState()
    class Success(val cap: Cap, val swap: Swap) : GetCapState()
}
