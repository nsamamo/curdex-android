package com.nichie.curdex.presentation.main.swap

sealed class SaveContactState {
    object Loading : SaveContactState()
    class ShowError(val message: String?) : SaveContactState()
    class Success(val message: String? = null, val isSend: Boolean = false) : SaveContactState()
}
