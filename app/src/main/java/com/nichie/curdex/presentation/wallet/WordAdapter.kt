package com.nichie.curdex.presentation.wallet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.nichie.curdex.AppExecutors
import com.nichie.curdex.BR
import com.nichie.curdex.R
import com.nichie.curdex.databinding.ItemWordBinding
import com.nichie.curdex.domain.model.Word
import com.nichie.curdex.presentation.base.DataBoundListAdapter

class WordAdapter(
    appExecutors: AppExecutors
) : DataBoundListAdapter<Word, ItemWordBinding>(
    appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Word>() {
        override fun areItemsTheSame(oldItem: Word, newItem: Word): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Word, newItem: Word): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun bind(binding: ItemWordBinding, item: Word) {
        binding.setVariable(BR.item, item)
        binding.executePendingBindings()
    }

    override fun createBinding(parent: ViewGroup, viewType: Int): ItemWordBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_word,
            parent,
            false
        )
}