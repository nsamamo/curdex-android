package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.FilterSetting

sealed class GetFilterSettingState {
    object Loading : GetFilterSettingState()
    class ShowError(val message: String?) : GetFilterSettingState()
    class Success(val filterSetting: FilterSetting) : GetFilterSettingState()
}
