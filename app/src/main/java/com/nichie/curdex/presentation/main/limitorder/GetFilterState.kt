package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.OrderFilter

sealed class GetFilterState {
    object Loading : GetFilterState()
    class ShowError(val message: String?) : GetFilterState()
    class Success(val orderFilter: OrderFilter) : GetFilterState()
}
