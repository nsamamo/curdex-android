package com.nichie.curdex.presentation.common

interface RefreshListener {
    fun onRefresh()
}