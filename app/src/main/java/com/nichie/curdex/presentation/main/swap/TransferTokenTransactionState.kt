package com.nichie.curdex.presentation.main.swap

import com.nichie.curdex.domain.model.ResponseStatus

sealed class TransferTokenTransactionState {
    object Loading : TransferTokenTransactionState()
    class ShowError(val message: String?) : TransferTokenTransactionState()
    class Success(val responseStatus: ResponseStatus) : TransferTokenTransactionState()
}
