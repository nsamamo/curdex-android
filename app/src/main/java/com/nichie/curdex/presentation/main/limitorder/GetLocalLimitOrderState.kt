package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.LocalLimitOrder

sealed class GetLocalLimitOrderState {
    object Loading : GetLocalLimitOrderState()
    class ShowError(val message: String?) : GetLocalLimitOrderState()
    class Success(val order: LocalLimitOrder) : GetLocalLimitOrderState()
}
