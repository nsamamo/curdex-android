package com.nichie.curdex.presentation.landing

import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.model.Word

sealed class GetMnemonicState {
    object Loading : GetMnemonicState()
    class ShowError(val message: String?) : GetMnemonicState()
    class Success(val wallet: Wallet, val words: List<Word>) : GetMnemonicState()
}
