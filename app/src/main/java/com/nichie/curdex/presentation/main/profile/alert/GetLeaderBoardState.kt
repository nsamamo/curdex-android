package com.nichie.curdex.presentation.main.profile.alert

import com.nichie.curdex.domain.model.Alert
import com.nichie.curdex.domain.model.CampaignInfo

sealed class GetLeaderBoardState {
    object Loading : GetLeaderBoardState()
    class ShowError(val message: String?) : GetLeaderBoardState()
    class Success(
        val alerts: List<Alert>,
        val campaignInfo: CampaignInfo,
        val lastCampaignTitle: String
    ) : GetLeaderBoardState()
}
