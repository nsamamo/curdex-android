package com.nichie.curdex.presentation.main.profile

import com.nichie.curdex.domain.model.ResponseStatus

sealed class ResetPasswordState {
    object Loading : ResetPasswordState()
    class ShowError(val message: String?) : ResetPasswordState()
    class Success(val status: ResponseStatus) : ResetPasswordState()
}
