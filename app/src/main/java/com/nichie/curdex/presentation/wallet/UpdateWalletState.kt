package com.nichie.curdex.presentation.wallet

import com.nichie.curdex.domain.model.Wallet

sealed class UpdateWalletState {
    object Loading : UpdateWalletState()
    class ShowError(val message: String?) : UpdateWalletState()
    class Success(val wallet: Wallet, val isWalletChangeEvent: Boolean = false) :
        UpdateWalletState()
}
