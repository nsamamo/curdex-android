package com.nichie.curdex.presentation.main.limitorder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nichie.curdex.domain.model.OrderFilter
import com.nichie.curdex.domain.usecase.limitorder.GetLimitOrdersFilterSettingUseCase
import com.nichie.curdex.domain.usecase.limitorder.SaveLimitOrderFilterUseCase
import com.nichie.curdex.domain.usecase.profile.GetLoginStatusUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.main.GetLoginStatusViewModel
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import javax.inject.Inject

class FilterViewModel @Inject constructor(
    private val saveLimitOrderFilterUseCase: SaveLimitOrderFilterUseCase,
    private val getLimitOrdersFilterSettingUseCase: GetLimitOrdersFilterSettingUseCase,
    val getLoginStatusUseCase: GetLoginStatusUseCase,
    private val errorHandler: ErrorHandler
) : GetLoginStatusViewModel(getLoginStatusUseCase, errorHandler) {

    private val _getFilterSettingCallback = MutableLiveData<Event<GetFilterSettingState>>()
    val getFilterSettingCallback: LiveData<Event<GetFilterSettingState>>
        get() = _getFilterSettingCallback

    private val _saveFilterStateCallback = MutableLiveData<Event<SaveFilterState>>()
    val saveFilterStateCallback: LiveData<Event<SaveFilterState>>
        get() = _saveFilterStateCallback


    fun getFilterSettings() {
        _getFilterSettingCallback.postValue(Event(GetFilterSettingState.Loading))
        getLimitOrdersFilterSettingUseCase.execute(
            Consumer {
                _getFilterSettingCallback.value = Event(GetFilterSettingState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getFilterSettingCallback.value =
                    Event(GetFilterSettingState.ShowError(errorHandler.getError(it)))
            },
            null
        )
    }

    override fun onCleared() {
        saveLimitOrderFilterUseCase.dispose()
        getLimitOrdersFilterSettingUseCase.dispose()
        super.onCleared()
    }

    fun saveOrderFilter(orderFilter: OrderFilter) {
        saveLimitOrderFilterUseCase.execute(
            Action {
                _saveFilterStateCallback.value = Event(SaveFilterState.Success(""))
            },
            Consumer {
                it.printStackTrace()
                _saveFilterStateCallback.value =
                    Event(SaveFilterState.ShowError(errorHandler.getError(it)))
            },
            SaveLimitOrderFilterUseCase.Param(orderFilter)
        )
    }
}