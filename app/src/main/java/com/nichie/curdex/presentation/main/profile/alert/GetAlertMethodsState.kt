package com.nichie.curdex.presentation.main.profile.alert

import com.nichie.curdex.domain.model.AlertMethods

sealed class GetAlertMethodsState {
    object Loading : GetAlertMethodsState()
    class ShowError(val message: String?) : GetAlertMethodsState()
    class Success(val alertMethods: AlertMethods) : GetAlertMethodsState()
}
