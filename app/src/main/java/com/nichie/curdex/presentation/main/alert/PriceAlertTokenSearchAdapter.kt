package com.nichie.curdex.presentation.main.alert

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.nichie.curdex.AppExecutors
import com.nichie.curdex.BR
import com.nichie.curdex.R
import com.nichie.curdex.databinding.ItemTokenSearchPriceAlertBinding
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.presentation.base.DataBoundListAdapter

class PriceAlertTokenSearchAdapter(
    appExecutors: AppExecutors,
    private val onTokenClick: ((Token) -> Unit)?
) : DataBoundListAdapter<Token, ItemTokenSearchPriceAlertBinding>(
    appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Token>() {
        override fun areItemsTheSame(oldItem: Token, newItem: Token): Boolean {
            return oldItem.tokenSymbol == newItem.tokenSymbol
        }

        override fun areContentsTheSame(oldItem: Token, newItem: Token): Boolean {
            return oldItem.areContentsTheSame(newItem)
        }
    }
) {

    fun submitFilterList(tokens: List<Token>) {
        if (itemCount > 0) {
            submitList(null)
            submitList(tokens)
        } else {
            submitList(tokens)
        }
        notifyDataSetChanged()

    }

    override fun bind(binding: ItemTokenSearchPriceAlertBinding, item: Token) {
        binding.root.setOnClickListener {
            onTokenClick?.invoke(item)
        }
        binding.setVariable(BR.token, item)
        binding.executePendingBindings()
    }

    override fun createBinding(parent: ViewGroup, viewType: Int): ItemTokenSearchPriceAlertBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_token_search_price_alert,
            parent,
            false
        )
}