package com.nichie.curdex.presentation.main.swap

sealed class SaveSwapState {
    object Loading : SaveSwapState()
    class ShowError(val message: String?) : SaveSwapState()
    class Success(val swap: String?) : SaveSwapState()
}
