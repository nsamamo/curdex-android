package com.nichie.curdex.presentation.main.balance

import com.nichie.curdex.domain.model.Wallet

sealed class GetAllWalletState {
    object Loading : GetAllWalletState()
    class ShowError(val message: String?) : GetAllWalletState()
    class Success(val wallets: List<Wallet>) : GetAllWalletState()
}
