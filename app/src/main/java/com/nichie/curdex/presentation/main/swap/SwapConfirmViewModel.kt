package com.nichie.curdex.presentation.main.swap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nichie.curdex.domain.model.Swap
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.usecase.swap.EstimateGasUseCase
import com.nichie.curdex.domain.usecase.swap.GetGasPriceUseCase
import com.nichie.curdex.domain.usecase.swap.GetSwapDataUseCase
import com.nichie.curdex.domain.usecase.swap.SwapTokenUseCase
import com.nichie.curdex.presentation.common.ADDITIONAL_SWAP_GAS_LIMIT
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.common.calculateDefaultGasLimit
import com.nichie.curdex.presentation.common.specialGasLimitDefault
import io.reactivex.functions.Consumer
import javax.inject.Inject

class SwapConfirmViewModel @Inject constructor(
    private val getSwapData: GetSwapDataUseCase,
    private val estimateGasUseCase: EstimateGasUseCase,
    private val getGasPriceUseCase: GetGasPriceUseCase,
    private val swapTokenUseCase: SwapTokenUseCase
) : ViewModel() {

    private val _getSwapCallback = MutableLiveData<Event<GetSwapState>>()
    val getSwapDataCallback: LiveData<Event<GetSwapState>>
        get() = _getSwapCallback

    private val _getGetGasLimitCallback = MutableLiveData<Event<GetGasLimitState>>()
    val getGetGasLimitCallback: LiveData<Event<GetGasLimitState>>
        get() = _getGetGasLimitCallback


    private val _swapTokenTransactionCallback =
        MutableLiveData<Event<SwapTokenTransactionState>>()
    val swapTokenTransactionCallback: LiveData<Event<SwapTokenTransactionState>>
        get() = _swapTokenTransactionCallback

    private val _getGetGasPriceCallback = MutableLiveData<Event<GetGasPriceState>>()
    val getGetGasPriceCallback: LiveData<Event<GetGasPriceState>>
        get() = _getGetGasPriceCallback


    fun getSwapData(wallet: Wallet) {
        getSwapData.execute(
            Consumer {
                _getSwapCallback.value = Event(GetSwapState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getSwapCallback.value = Event(GetSwapState.ShowError(it.localizedMessage))
            },
            GetSwapDataUseCase.Param(wallet)
        )
    }

    fun swap(wallet: Wallet?, swap: Swap?) {
        swap?.let {
            _swapTokenTransactionCallback.postValue(Event(SwapTokenTransactionState.Loading))
            swapTokenUseCase.execute(
                Consumer {
                    _swapTokenTransactionCallback.value =
                        Event(SwapTokenTransactionState.Success(it))
                },
                Consumer {
                    it.printStackTrace()
                    _swapTokenTransactionCallback.value =
                        Event(SwapTokenTransactionState.ShowError(it.localizedMessage))
                },
                SwapTokenUseCase.Param(wallet!!, swap)

            )
        }
    }

    fun getGasPrice() {
        getGasPriceUseCase.dispose()
        getGasPriceUseCase.execute(
            Consumer {
                _getGetGasPriceCallback.value = Event(GetGasPriceState.Success(it))
            },
            Consumer {
                it.printStackTrace()
            },
            null
        )
    }

    fun getGasLimit(wallet: Wallet?, swap: Swap?) {
        if (wallet == null || swap == null) return
        estimateGasUseCase.dispose()
        estimateGasUseCase.execute(
            Consumer {
                if (it.error == null) {

                    val gasLimit = calculateDefaultGasLimit(swap.tokenSource, swap.tokenDest)
                        .min(it.amountUsed.multiply(120.toBigInteger()).divide(100.toBigInteger()) + ADDITIONAL_SWAP_GAS_LIMIT.toBigInteger())

                    val specialGasLimit = specialGasLimitDefault(swap.tokenSource, swap.tokenDest)

                    _getGetGasLimitCallback.value = Event(
                        GetGasLimitState.Success(
                            if (specialGasLimit != null) {
                                specialGasLimit.max(gasLimit)
                            } else {
                                gasLimit
                            }
                        )
                    )
                }

            },
            Consumer {
                it.printStackTrace()
            },
            EstimateGasUseCase.Param(
                wallet,
                swap.tokenSource,
                swap.tokenDest,
                swap.sourceAmount,
                swap.minConversionRate
            )
        )
    }

}