package com.nichie.curdex.presentation.main.profile.kyc

import com.nichie.curdex.domain.model.KycResponseStatus

sealed class ReSubmitState {
    object Loading : ReSubmitState()
    class ShowError(val message: String?) : ReSubmitState()
    class Success(val status: KycResponseStatus) : ReSubmitState()
}
