package com.nichie.curdex.presentation.main.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nichie.curdex.domain.model.FilterItem
import com.nichie.curdex.domain.model.TransactionFilter
import com.nichie.curdex.domain.usecase.token.GetTokenListUseCase
import com.nichie.curdex.domain.usecase.transaction.GetTransactionFilterUseCase
import com.nichie.curdex.domain.usecase.transaction.SaveTransactionFilterUseCase
import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.main.SelectedWalletViewModel
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import javax.inject.Inject

class TransactionFilterViewModel @Inject constructor(
    private val getTokenListUseCase: GetTokenListUseCase,
    private val getTransactionFilterUseCase: GetTransactionFilterUseCase,
    private val saveTransactionFilterUseCase: SaveTransactionFilterUseCase,
    getSelectedWalletUseCase: GetSelectedWalletUseCase,
    private val errorHandler: ErrorHandler
) : SelectedWalletViewModel(getSelectedWalletUseCase, errorHandler) {
    private val _getTransactionFilterCallback = MutableLiveData<Event<GetTransactionFilterState>>()
    val getTransactionFilterCallback: LiveData<Event<GetTransactionFilterState>>
        get() = _getTransactionFilterCallback

    private val _saveTransactionFilterCallback =
        MutableLiveData<Event<SaveTransactionFilterState>>()
    val saveTransactionFilterCallback: LiveData<Event<SaveTransactionFilterState>>
        get() = _saveTransactionFilterCallback

    private fun getTokenList(address: String, transactionFilter: TransactionFilter) {
        getTokenListUseCase.execute(
            Consumer {
                val items = it.sortedBy { it.tokenSymbol }.map { token ->
                    FilterItem(transactionFilter.tokens.find {
                        it == token.tokenSymbol
                    } != null, token.tokenSymbol)
                }
                _getTransactionFilterCallback.value = Event(
                    GetTransactionFilterState.Success(
                        transactionFilter, items
                    )
                )
            },
            Consumer {
                it.printStackTrace()
                _getTransactionFilterCallback.value =
                    Event(
                        GetTransactionFilterState.ShowError(
                            errorHandler.getError(it)
                        )
                    )
            },
            address
        )
    }

    fun getTransactionFilter(address: String) {
        getTransactionFilterUseCase.execute(
            Consumer {
                getTokenList(address, it)
            },
            Consumer {

            },
            GetTransactionFilterUseCase.Param(address)
        )
    }

    fun saveTransactionFilter(transactionFilter: TransactionFilter) {
        saveTransactionFilterUseCase.execute(
            Action {
                _saveTransactionFilterCallback.value = Event(SaveTransactionFilterState.Success(""))
            },
            Consumer {
                it.printStackTrace()
                _saveTransactionFilterCallback.value =
                    Event(SaveTransactionFilterState.ShowError(errorHandler.getError(it)))
            },
            SaveTransactionFilterUseCase.Param(transactionFilter)
        )
    }
}