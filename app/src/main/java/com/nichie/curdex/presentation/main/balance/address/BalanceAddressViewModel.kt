package com.nichie.curdex.presentation.main.balance.address

import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.presentation.main.SelectedWalletViewModel
import com.nichie.curdex.util.ErrorHandler
import javax.inject.Inject

class BalanceAddressViewModel @Inject constructor(
    getSelectedWalletUseCase: GetSelectedWalletUseCase,
    errorHandler: ErrorHandler
) : SelectedWalletViewModel(getSelectedWalletUseCase, errorHandler)