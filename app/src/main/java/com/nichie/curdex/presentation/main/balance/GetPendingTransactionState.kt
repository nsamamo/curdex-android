package com.nichie.curdex.presentation.main.balance

import com.nichie.curdex.domain.model.Transaction

sealed class GetPendingTransactionState {
    object Loading : GetPendingTransactionState()
    class ShowError(val message: String?) : GetPendingTransactionState()
    class Success(val transactions: List<Transaction>) : GetPendingTransactionState()
}
