package com.nichie.curdex.presentation.setting

import com.nichie.curdex.domain.model.PassCode

sealed class GetPinState {
    object Loading : GetPinState()
    class ShowError(val message: String?) : GetPinState()
    class Success(val passCode: PassCode) : GetPinState()
}
