package com.nichie.curdex.presentation.main.limitorder

import com.nichie.curdex.domain.model.Order


sealed class OrderItem {
    class Header(val date: String?) : OrderItem()
    class ItemEven(val order: Order) : OrderItem()
    class ItemOdd(val order: Order) : OrderItem()
}