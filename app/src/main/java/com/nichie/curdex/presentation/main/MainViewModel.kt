package com.nichie.curdex.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.model.Transaction
import com.nichie.curdex.domain.model.Wallet
import com.nichie.curdex.domain.usecase.balance.UpdateBalanceUseCase
import com.nichie.curdex.domain.usecase.profile.GetLoginStatusUseCase
import com.nichie.curdex.domain.usecase.token.GetBalancePollingUseCase
import com.nichie.curdex.domain.usecase.token.GetOtherBalancePollingUseCase
import com.nichie.curdex.domain.usecase.token.GetOtherTokenBalancesUseCase
import com.nichie.curdex.domain.usecase.token.GetTokenBalanceUseCase
import com.nichie.curdex.domain.usecase.token.GetTokensBalanceUseCase
import com.nichie.curdex.domain.usecase.transaction.GetPendingTransactionsUseCase
import com.nichie.curdex.domain.usecase.transaction.GetTransactionsPeriodicallyUseCase
import com.nichie.curdex.domain.usecase.transaction.MonitorPendingTransactionUseCase
import com.nichie.curdex.domain.usecase.wallet.CreateWalletUseCase
import com.nichie.curdex.domain.usecase.wallet.GetAllWalletUseCase
import com.nichie.curdex.domain.usecase.wallet.GetSelectedWalletUseCase
import com.nichie.curdex.domain.usecase.wallet.UpdateSelectedWalletUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.presentation.landing.CreateWalletState
import com.nichie.curdex.presentation.main.balance.GetAllWalletState
import com.nichie.curdex.presentation.main.balance.GetPendingTransactionState
import com.nichie.curdex.presentation.main.profile.UserInfoState
import com.nichie.curdex.presentation.wallet.UpdateWalletState
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getAllWalletUseCase: GetAllWalletUseCase,
    private val getPendingTransactionsUseCase: GetPendingTransactionsUseCase,
    private val monitorPendingTransactionsUseCase: MonitorPendingTransactionUseCase,
    getWalletUseCase: GetSelectedWalletUseCase,
    private val createWalletUseCase: CreateWalletUseCase,
    private val updateSelectedWalletUseCase: UpdateSelectedWalletUseCase,
    private val getLoginStatusUseCase: GetLoginStatusUseCase,
    private val getBalancePollingUseCase: GetBalancePollingUseCase,
    private val getOtherBalancePollingUseCase: GetOtherBalancePollingUseCase,
    private val getBatchTokenBalanceUseCase: GetTokensBalanceUseCase,
    private val forceBatchTokenBalanceUseCase: GetTokensBalanceUseCase,
    private val getTokenBalanceUseCase: GetTokenBalanceUseCase,
    private val getOtherTokenBalancesUseCase: GetOtherTokenBalancesUseCase,
    private val getTransactionsPeriodicallyUseCase: GetTransactionsPeriodicallyUseCase,
    private val updateBalanceUseCase: UpdateBalanceUseCase,
    private val errorHandler: ErrorHandler
) : SelectedWalletViewModel(getWalletUseCase, errorHandler) {

    private val _getAllWalletStateCallback = MutableLiveData<Event<GetAllWalletState>>()
    val getAllWalletStateCallback: LiveData<Event<GetAllWalletState>>
        get() = _getAllWalletStateCallback

    private val _getPendingTransactionStateCallback =
        MutableLiveData<Event<GetPendingTransactionState>>()
    val getPendingTransactionStateCallback: LiveData<Event<GetPendingTransactionState>>
        get() = _getPendingTransactionStateCallback

    private val _getMnemonicCallback = MutableLiveData<Event<CreateWalletState>>()
    val createWalletCallback: LiveData<Event<CreateWalletState>>
        get() = _getMnemonicCallback

    private val _switchWalletCompleteCallback = MutableLiveData<Event<UpdateWalletState>>()
    val switchWalletCompleteCallback: LiveData<Event<UpdateWalletState>>
        get() = _switchWalletCompleteCallback

    private val _getLoginStatusCallback = MutableLiveData<Event<UserInfoState>>()
    val getLoginStatusCallback: LiveData<Event<UserInfoState>>
        get() = _getLoginStatusCallback

    private var currentPendingList: List<Transaction> = listOf()

    fun getLoginStatus() {
        getLoginStatusUseCase.dispose()
        getLoginStatusUseCase.execute(
            Consumer {
                _getLoginStatusCallback.value = Event(UserInfoState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getLoginStatusCallback.value =
                    Event(UserInfoState.ShowError(errorHandler.getError(it)))
            },
            null
        )
    }


    fun getWallets() {
        getAllWalletUseCase.execute(
            Consumer {
                _getAllWalletStateCallback.value = Event(
                    GetAllWalletState.Success(
                        it
                    )
                )
            },
            Consumer {
                it.printStackTrace()
                _getAllWalletStateCallback.value =
                    Event(
                        GetAllWalletState.ShowError(
                            errorHandler.getError(it)
                        )
                    )
            },
            null
        )
    }

    fun pollingTokenBalance(wallets: List<Wallet>, selectedWallet: Wallet) {
        monitorListedTokenBalance(wallets, selectedWallet)
        monitorOtherTokenBalance()
    }

    private fun monitorListedTokenBalance(wallets: List<Wallet>, selectedWallet: Wallet) {
        getBalancePollingUseCase.dispose()
        getBalancePollingUseCase.execute(
            Consumer {
                loadBalances(Pair(selectedWallet, it))
            },
            Consumer {
                it.printStackTrace()
            },
            GetBalancePollingUseCase.Param(wallets)
        )
    }

    private fun monitorOtherTokenBalance() {
        getOtherBalancePollingUseCase.dispose()
        getOtherBalancePollingUseCase.execute(
            Consumer {
                loadOtherBalances(it)
            },
            Consumer {
                it.printStackTrace()
                Timber.e(it.localizedMessage)
            },
            GetOtherBalancePollingUseCase.Param()

        )
    }

    private fun loadOtherBalances(others: List<Token>) {
        others.sortedByDescending { it.currentBalance }.forEach { token ->
            getTokenBalanceUseCase.execute(
                Action {

                },
                Consumer {
                    Timber.e(it.localizedMessage)
                    it.printStackTrace()
                },
                token
            )
        }

//        Timber.e("loadOtherBalances")
//        getOtherTokenBalancesUseCase.dispose()
//        getOtherTokenBalancesUseCase.execute(
//            Action {
//            },
//            Consumer {
//                it.printStackTrace()
//                Timber.e(it.localizedMessage)
//            }
//            , GetOtherTokenBalancesUseCase.Param(others))
    }

    fun getTransactionPeriodically(wallet: Wallet) {
        getTransactionsPeriodicallyUseCase.dispose()
        getTransactionsPeriodicallyUseCase.execute(
            Consumer { },
            Consumer {
                it.printStackTrace()
            },
            GetTransactionsPeriodicallyUseCase.Param(wallet)
        )
    }

    fun getPendingTransaction(wallet: Wallet) {
        getPendingTransactionsUseCase.dispose()
        getPendingTransactionsUseCase.execute(
            Consumer {
                if (it.isNotEmpty() && it != currentPendingList) {
                    currentPendingList = it
                    monitorPendingTransactionsUseCase.dispose()
                    monitorPendingTransactionsUseCase.execute(
                        Consumer { tx ->
                            _getPendingTransactionStateCallback.value = Event(
                                GetPendingTransactionState.Success(tx)
                            )

                            if (tx.none { it.blockNumber.isEmpty() }) {
                                monitorPendingTransactionsUseCase.dispose()
                            }
                        },
                        Consumer { ex ->
                            ex.printStackTrace()
                            Timber.e(ex.localizedMessage)
                        },
                        MonitorPendingTransactionUseCase.Param(it, wallet)
                    )
                } else {
                    _getPendingTransactionStateCallback.value = Event(
                        GetPendingTransactionState.Success(
                            it
                        )
                    )
                }

            },
            Consumer {
                it.printStackTrace()
                Timber.e(it.localizedMessage)
                _getPendingTransactionStateCallback.value = Event(
                    GetPendingTransactionState.ShowError(
                        errorHandler.getError(it)
                    )
                )
            },
            wallet.address
        )
    }

    fun createWallet(walletName: String = "Untitled") {
        _getMnemonicCallback.postValue(Event(CreateWalletState.Loading))
        createWalletUseCase.execute(
            Consumer {
                _getMnemonicCallback.value =
                    Event(CreateWalletState.Success(it.first, it.second))
            },
            Consumer {
                it.printStackTrace()
                _getMnemonicCallback.value =
                    Event(CreateWalletState.ShowError(errorHandler.getError(it)))
            },
            CreateWalletUseCase.Param(walletName)
        )
    }

    private fun loadBalances(
        pair: Pair<Wallet, List<Token>>
    ) {
        getBatchTokenBalanceUseCase.dispose()
        getBatchTokenBalanceUseCase.execute(
            Action {
                //                _switchWalletCompleteCallback.value =
//                    Event(UpdateWalletState.Success(pair.first, isWalletChangedEvent))
                updateBalance(pair.first)
            },
            Consumer {
                Timber.e(it.localizedMessage)
                it.printStackTrace()
            },
            GetTokensBalanceUseCase.Param(pair.first, pair.second)
        )
    }

    private fun updateBalance(wallet: Wallet) {
        updateBalanceUseCase.execute(
            Action {

            },
            Consumer {

            },
            UpdateBalanceUseCase.Param(wallet)
        )
    }

    private fun forceUpdateBalance(
        pair: Pair<Wallet, List<Token>>
    ) {
        forceBatchTokenBalanceUseCase.dispose()
        forceBatchTokenBalanceUseCase.execute(
            Action {
                _switchWalletCompleteCallback.value =
                    Event(UpdateWalletState.Success(pair.first, true))
                updateBalance(pair.first)
            },
            Consumer {
                Timber.e(it.localizedMessage)
                it.printStackTrace()
                _switchWalletCompleteCallback.value =
                    Event(UpdateWalletState.ShowError(errorHandler.getError(it)))
            },
            GetTokensBalanceUseCase.Param(pair.first, pair.second)
        )
    }

    fun updateSelectedWallet(wallet: Wallet) {
        updateSelectedWalletUseCase.dispose()
        _switchWalletCompleteCallback.postValue(Event(UpdateWalletState.Loading))
        updateSelectedWalletUseCase.execute(
            Consumer { wl ->
                forceUpdateBalance(wl)

            },
            Consumer {
                it.printStackTrace()
                _switchWalletCompleteCallback.value =
                    Event(UpdateWalletState.ShowError(errorHandler.getError(it)))
            },
            UpdateSelectedWalletUseCase.Param(wallet)
        )
    }
}