package com.nichie.curdex.presentation.main.alert

import com.nichie.curdex.domain.model.Alert

sealed class CreateOrUpdateAlertState {
    object Loading : CreateOrUpdateAlertState()
    class ShowError(val message: String?) : CreateOrUpdateAlertState()
    class Success(val alert: Alert) : CreateOrUpdateAlertState()
}
