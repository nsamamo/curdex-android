package com.nichie.curdex.presentation.main.profile

import com.nichie.curdex.domain.model.ResponseStatus

sealed class SignUpState {
    object Loading : SignUpState()
    class ShowError(val message: String?) : SignUpState()
    class Success(val registerStatus: ResponseStatus) : SignUpState()
}
