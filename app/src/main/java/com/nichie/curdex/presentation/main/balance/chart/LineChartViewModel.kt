package com.nichie.curdex.presentation.main.balance.chart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nichie.curdex.domain.model.Token
import com.nichie.curdex.domain.usecase.token.GetChartDataForTokenUseCase
import com.nichie.curdex.presentation.common.Event
import com.nichie.curdex.util.ErrorHandler
import io.reactivex.functions.Consumer
import javax.inject.Inject

class LineChartViewModel @Inject constructor(
    private val getChartDataForTokenUseCase: GetChartDataForTokenUseCase,
    private val errorHandler: ErrorHandler
) : ViewModel() {

    private val _getChartCallback = MutableLiveData<Event<GetChartState>>()
    val getChartCallback: LiveData<Event<GetChartState>>
        get() = _getChartCallback

    fun getChartData(token: Token?, chartType: ChartType?) {
        if (token == null || chartType == null) return
        getChartDataForTokenUseCase.execute(
            Consumer {
                _getChartCallback.value = Event(GetChartState.Success(it))
            },
            Consumer {
                it.printStackTrace()
                _getChartCallback.value = Event(GetChartState.ShowError(errorHandler.getError(it)))
            },
            GetChartDataForTokenUseCase.Param(token, chartType)
        )

    }
}