package com.nichie.curdex.presentation.common

interface LoginState {

    fun getLoginStatus()
}